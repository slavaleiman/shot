#include "unity.h"
#include <stdint.h>

#include "cmsis_os.h"
#include "ai_control.h"

extern ai_control_t ai_control;
void setUp(void)
{
  	ai_init();
  	ai_set_defaults();
}

void tearDown(void)
{
}

void vPortEnterCritical( void ){}
void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState){}
void delayUS(uint16_t d){}
SPI_HandleTypeDef hspi3;
HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout){}
void errors_on_error(int err){}
void vPortExitCritical( void ){}
osStatus osDelay(uint32_t millisec){}
void shot_gui_update_voltage(void){}
void options_update(opt_list_t* opt_list){}
void options_new_int(opt_list_t* opt_list, option_t* opt, const char* name, const char* units, int (*cb)(option_t*, int8_t), bool is_coeff){}
void options_new_float(opt_list_t* opt_list, option_t* opt, const char* name, const char* units, float (*cb)(option_t*, int8_t), bool is_coeff){}
void options_init(opt_list_t* opt_list){}

void test_mean_voltage(void)
{
	uint8_t channel = 0;
	float val = 1.5;
	for(int i = 0; i < VOLTAGE_BUFFER_SIZE; ++i)
	{
		val += 0.01;
		ai_control.voltage_buffer[i] = val;
		++ai_control.voltage_buffer_fillness;
	}
	TEST_ASSERT_TRUE(0 != ai_mean_output_voltage(0));
	TEST_ASSERT_TRUE(0 != ai_mean_output_voltage(30));
	TEST_ASSERT_TRUE(0 != ai_mean_output_voltage(30000));
	TEST_ASSERT_TRUE(0 != ai_mean_output_voltage(-1));
	TEST_ASSERT_TRUE(0 != ai_mean_output_voltage(-203));
}

void test_mean_amperage(void)
{
	float val = 1.5;
	for(int i = 0; i < AMPERAGE_BUFFER_SIZE; ++i)
	{
		val += 0.01;
		ai_control.amperage_buffer[i] = val;
		++ai_control.amperage_buffer_fillness;
	}
	TEST_ASSERT_TRUE(0 != ai_mean_charge_amperage(0));
	TEST_ASSERT_TRUE(0 != ai_mean_charge_amperage(30));
	TEST_ASSERT_TRUE(0 != ai_mean_charge_amperage(30000));
	TEST_ASSERT_TRUE(0 != ai_mean_charge_amperage(-1));
	TEST_ASSERT_TRUE(0 != ai_mean_charge_amperage(-400));
}

void test_adc_value_sum_eq(void)
{
	uint8_t channel = 0;
	for(int i = 0; i < AI_NUM_SAMPLES; ++i)
	{
		ai_control.adc_value[channel][i] = 30000;
		ai_control.adc_value[21][i] = 30000;
	}
	TEST_ASSERT_EQUAL_UINT32(30000 * AI_NUM_SAMPLES, ai_adc_value_summ(channel));
	TEST_ASSERT_TRUE(0 == ai_adc_value_summ(-1));
	TEST_ASSERT_TRUE(0 != ai_adc_value_summ(21));
}

void fill_adc_by(uint8_t channel, uint32_t val)
{
	for(int i = 0; i < AI_NUM_SAMPLES; ++i)
	{
		ai_control.adc_value[channel][i] = val;
	}
}

void test_accum_voltage_0(void)
{
	uint8_t channel = 0;
	fill_adc_by(channel, CALIB_COEF_ZERO_VOLTAGE_DEFAULT);
	update_value(channel);
	TEST_ASSERT_EQUAL_INT32(0, ai_control.accum_voltage[channel]);
}

void test_accum_voltage(void)
{
	uint8_t channel = 0;
	fill_adc_by(channel, 0x7FE);
	update_value(channel);
	TEST_ASSERT_LESS_THAN_INT32(5, ai_control.accum_voltage[channel]);
}

void test_battery_voltage(void)
{
	uint8_t channel = 17;
	fill_adc_by(channel, 0x802);
	update_value(channel);
	TEST_ASSERT_LESS_THAN_INT32(5, ai_control.battery_voltage);

	fill_adc_by(channel, 0xFFF);
	update_value(channel);
	TEST_ASSERT_GREATER_THAN_INT32(5, ai_control.battery_voltage);
}

void test_output_voltage(void)
{
	uint8_t channel = 18;
	fill_adc_by(channel, 0x802);
	update_value(channel);
	TEST_ASSERT_LESS_THAN_INT32(5, ai_control.output_voltage);

	fill_adc_by(channel, 0xFFF);
	update_value(channel);
	TEST_ASSERT_GREATER_THAN_INT32(5, ai_control.output_voltage);
}

void test_charge_amperage(void)
{
	uint8_t channel = 19;
	fill_adc_by(channel, 0x802);
	update_value(channel);
	TEST_ASSERT_LESS_THAN_INT32(5, ai_control.charge_amperage);

	fill_adc_by(channel, 0xFFF);
	update_value(channel);
	TEST_ASSERT_GREATER_THAN_INT32(5, ai_control.charge_amperage);
}

void test_output_amperage(void)
{
	uint8_t channel = 20;
	fill_adc_by(channel, 0x802);
	update_value(channel);
	TEST_ASSERT_LESS_THAN_INT32(5, ai_control.output_amperage);

	fill_adc_by(channel, 0xFFF);
	update_value(channel);
	TEST_ASSERT_GREATER_THAN_INT32(5, ai_control.output_amperage);
}

void test_switch_relay(void)
{
#define BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

	printf("  ODR3         ODR2        ODR1\n");
	for(uint8_t ch = 0; ch < 21; ++ch)
	{
		switch_relay(ch);
		printf(" %2d "BINARY_PATTERN" "BINARY_PATTERN BINARY_PATTERN" "BINARY_PATTERN BINARY_PATTERN" \n", ch, BINARY(OUTPUT_PORT3), BINARY(OUTPUT_PORT2>>8), BINARY(OUTPUT_PORT2), BINARY(OUTPUT_PORT1>>8), BINARY(OUTPUT_PORT1));
	}
}

int main(void)
{
	UNITY_BEGIN();
	RUN_TEST(test_mean_voltage);
	RUN_TEST(test_mean_amperage);
	RUN_TEST(test_adc_value_sum_eq);
	RUN_TEST(test_accum_voltage_0);
	RUN_TEST(test_accum_voltage);
	RUN_TEST(test_battery_voltage);
	RUN_TEST(test_output_voltage);
	RUN_TEST(test_charge_amperage);
	RUN_TEST(test_output_amperage);
	RUN_TEST(test_switch_relay);

	return UNITY_END();
}
