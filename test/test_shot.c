#include "unity.h"
#include <stdint.h>
#include <string.h>

#include "cmsis_os.h"
#include "shot.h"
#include "ai_control.h"

#define BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

uint8_t PCFDATA[5] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

bool ds2482_iserror = false;

extern shot_t shot;
float Ubat; // напряжение на всей батарее
float Uacc; // напряжение на одном аккуме
float Uout; // напряжение на выходе шкафа
float Iout; // ток на выходе
float Ibat; // ток заряда

void setUp(void) // called before every test
{
	shot_init();
	shot.measure_status = 0;
}

void tearDown(void)
{
}

uint32_t ai_adc_value_summ(uint8_t channel){}
float ai_battery_voltage(void){return Ubat;}
float ai_charge_amperage(void){return Ibat;}
float ai_accum_voltage(uint8_t idx){return Uacc;}
void ai_init(void){}
float ai_Izmin(void){return MINIMAL_CHARGE_AMPERAGE;}
float ai_mean_charge_amperage(uint16_t period){return Ibat;}
float ai_mean_output_voltage(uint16_t period){return Uout;}

void ai_set_defaults(void){}
uint32_t HAL_GetTick(void){}
int8_t  pcf_configure(uint8_t mcu, uint8_t port){}
// CALL ONLY AFTER pcf_check !!!
int8_t  pcf_read(uint8_t mcu, uint8_t pin){return (PCFDATA[mcu] & (1 << pin)) ? 1 : 0;}
uint8_t pcf_status(void){}
void    pcf_toggle(uint8_t mcu, uint8_t pin){}
void    pcf_write(uint8_t mcu, uint8_t pin, uint8_t state)
{
	if(state)PCFDATA[mcu] |= (1 << pin);
	else PCFDATA[mcu] &= ~(1 << pin);
}

void rom_write_time(void){}
soft_timer_t* soft_timer(uint32_t period, void (*cb)(void), bool is_one_shot, bool do_remove){}
void soft_timer_init(void){}
void soft_timer_restart(soft_timer_t* timer){}
void soft_timer_stop(soft_timer_t* t){}
bool soft_timer_is_active(soft_timer_t* timer){}

uint8_t is_temp_error = 0;
bool tsensor_check_temp(void){return is_temp_error;}
bool tsensor_is_overheat(void){return is_temp_error;}
bool tsensor_is_overrange(void){return is_temp_error;}
void tsensors_init(void){}

void ai_battery_capacity_inc(float a){}
void ai_battery_capacity_dec(float a){}
bool ds2482_is_error(void){return ds2482_iserror;}

void battery_off(void)
{
	PCFDATA[3] |= (1 << 7); // PCF_BATTERY_CONNECT
}

void battery_on(void)
{
	PCFDATA[3] &= ~(1 << 7); // PCF_BATTERY_CONNECT
	PCFDATA[4] &= ~(1 << 4); // input signal battery enabled
}

void reset_all(void)
{
	is_temp_error = 0;
	memset(PCFDATA, 0xff, 5);
	shot.battery.status = BAT_INIT;
	shot.measure_status = 0;
	shot.control_status = 0;
	shot.accum_overcharge = 0;
	battery_on();
	ds2482_iserror = false;
	Ubat = 206;
	Uacc = 11;
	Uout = 191; // условие для включения батареи
	Ibat = 1.8;	// ток через батарею
	Iout = 3;	// ток на выходе шкафа
}

void process_all(void)
{
	shot_process_input();
    shot_process_output();
    shot_check_accum_voltage();
    shot_check_temp();
    shot_process_status();
    shot_process_mode();
    shot_battery_status_update();

	shot_process_input();
    shot_process_output();
    shot_check_accum_voltage();
    shot_check_temp();
    shot_process_status();
    shot_process_mode();
    shot_battery_status_update();
}

void print_status(void)
{
	printf("PCF[0] "BINARY_PATTERN"\n", BINARY(PCFDATA[0]));
	printf("PCF[1] "BINARY_PATTERN"\n", BINARY(PCFDATA[1]));
	printf("PCF[2] "BINARY_PATTERN"\n", BINARY(PCFDATA[2]));
	printf("PCF[3] "BINARY_PATTERN"\n", BINARY(PCFDATA[3]));
	printf("PCF[4] "BINARY_PATTERN"\n", BINARY(PCFDATA[4]));

	printf("measure_status "BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.measure_status>>8), BINARY(shot.measure_status));
	printf("control_status "BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.control_status>>8), BINARY(shot.control_status));
	printf("battery status: %d\n", shot.battery.status);
	printf("mode: %d\n", shot.mode);
}

void test_common_state(void)
{
	reset_all();
	process_all();
	print_status();
	// TEST_ASSERT_BITS_HIGH(TEMP_SENSOR_WARNING_FLAG, shot.measure_status);
	// TEST_ASSERT_EQUAL_UINT(CHECK_BATTERY, shot.mode);
}

void test_temp(void)
{
	reset_all();
	shot.mode = CHECK_BATTERY;
	is_temp_error = TEMP_SENSOR_WARNING_FLAG;
	process_all();
	TEST_ASSERT_BITS_HIGH(TEMP_SENSOR_WARNING_FLAG, shot.measure_status);
	TEST_ASSERT_EQUAL_UINT(CHECK_BATTERY, shot.mode);

	reset_all();
	shot.mode = CHARGING;
	is_temp_error = TEMP_SENSOR_WARNING_FLAG;
	process_all();
	TEST_ASSERT_BITS_HIGH(TEMP_SENSOR_WARNING_FLAG, shot.measure_status);
	TEST_ASSERT_EQUAL_UINT(MAINTENANCE, shot.mode);

	reset_all();
	shot.mode = CHARGING;
	is_temp_error = TEMP_SENSOR_ERROR_FLAG;
	process_all();
	TEST_ASSERT_BITS_HIGH(TEMP_SENSOR_ERROR_FLAG, shot.measure_status);
	TEST_ASSERT_EQUAL_UINT(BAT_SWITCHED_OFF, shot.battery.status);

	reset_all();
	shot.mode = MAINTENANCE;
	Ibat = 2.0;
	process_all();
	printf(BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.measure_status>>8), BINARY(shot.measure_status));
	printf("mode = %u\n", shot.mode);
	TEST_ASSERT_EQUAL_UINT(CHARGING, shot.mode);

	reset_all();
	shot.mode = CHARGING;
	Uout = 180;
	process_all();
	TEST_ASSERT_EQUAL_UINT(BAT_SWITCHED_OFF, shot.battery.status);
}

void test_accum(void)
{
	reset_all();
	Ibat = -1;
	Ubat = 182;
	shot.mode = MAINTENANCE;
	process_all();
	printf("measure_status "BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.measure_status>>8), BINARY(shot.measure_status));
	TEST_ASSERT_EQUAL_UINT(CHARGING, shot.mode);
	TEST_ASSERT_BITS_HIGH(BATTERY_LOW_VOLTAGE_FLAG, shot.measure_status);

	reset_all();
	Uacc = 23.0;
	shot.mode = MAINTENANCE;
	process_all();
	printf("measure_status "BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.measure_status>>8), BINARY(shot.measure_status));
	printf("mode %d\n", shot.mode);
	TEST_ASSERT_EQUAL_UINT(MAINTENANCE, shot.mode);
	TEST_ASSERT_BITS_HIGH(ACCUM_OVERCHARGE_FLAG, shot.measure_status);
}

void test_bat_status(void)
{
	// BAT_NOT_CONNECTED,  	// Батарея не включилась 	DO15=вкл && DI20=1
	// BAT_SWITCHED_OFF, 	// Батарея отключена	 	DO15=выкл
	// BAT_BROKEN,			// Батарея оборвана			DO15=1 && Iзаряда=[-0.5..+0.5]B
	// BAT_LOW,				// Батарея разряжена 		U < 183 В,
	// BAT_DISCHARGING,		// Батарея разряжается		Iзаряда<-0.1A,
	// BAT_CHARGING,		// Батарея заряжается		режим заряда
	// BAT_MAINTENANCE, 	// Режим сохранения 		режим сохранения
	// BAT_CHECK,			// Проверка батареи 		режим проверки

	reset_all();
	Ibat = 0.6;
	Ubat = 206;
	Uout = 206;
	battery_off();
	process_all();
	printf("battery status: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_SWITCHED_OFF, shot.battery.status);

	reset_all();
	Uacc = 11.0;
	Ibat = 0.1;
	process_all();
	printf("battery broken: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_BROKEN, shot.battery.status);

	reset_all();
	Ibat = 0.6;
	Ubat = 204;
	process_all();
	printf("battery low: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_LOW, shot.battery.status);

	reset_all();
	Ibat = -0.5;
	Ubat = 205;
	process_all();
	printf("battery discharge: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_DISCHARGING, shot.battery.status);

	reset_all();
	Ibat = 2;
	Ubat = 205;
	shot.mode = MAINTENANCE;
	process_all();
	printf("measure_status "BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.measure_status>>8), BINARY(shot.measure_status));
	printf("battery charge: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_CHARGING, shot.battery.status);

	reset_all();
	Ibat = 1.2;
	Uacc = 11.2;
	Ubat = 206;
	Uout = 207;
	shot.mode = CHARGING;
	process_all();
	printf("measure_status "BINARY_PATTERN""BINARY_PATTERN"\n", BINARY(shot.measure_status>>8), BINARY(shot.measure_status));
	printf("battery maintenance: %d\n", shot.battery.status);
	TEST_ASSERT_TRUE(shot.battery.status == BAT_MAINTENANCE);

	reset_all();
	shot.battery.status = BAT_CHECK;
	Ibat = 1.5;
	Ubat = 205;
	shot.mode = CHECK_BATTERY;
	process_all();
	printf("battery check: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_CHECK, shot.battery.status);
	TEST_ASSERT_BITS_LOW(BATTERY_LOW_VOLTAGE_FLAG, shot.measure_status);

	reset_all();
	shot.battery.status = BAT_CHECK;
	ds2482_iserror = true;
	process_all();
	printf("battery check: %d\n", shot.battery.status);
	TEST_ASSERT_EQUAL_UINT(BAT_SWITCHED_OFF, shot.battery.status);
	TEST_ASSERT_BITS_LOW(BATTERY_LOW_VOLTAGE_FLAG, shot.measure_status);
}

#define LAMP_FAULT_PIN (1 << 3)
void test_avaria(void)
{
	reset_all();
	for(uint8_t mcu = 0; mcu <= 4; mcu += 2)
	{
		for(uint8_t pin = 0; pin < 8; ++pin)
		{
			if(((mcu == 2) && ((pin == 1) || (pin == 2) || (pin == 3) || (pin == 4)))
			|| ((mcu == 4) && ((pin == 0) || (pin >= 4))))
				continue;

			PCFDATA[mcu] &= ~(1 << pin);
			if((mcu == 2) && (pin == 7))
				 PCFDATA[4] &= ~(1 << 0); // for insul pin test

			// set low any input -> fault
			process_all();
			printf("test low %d-%d\n", mcu, pin);
			TEST_ASSERT_BITS_LOW(LAMP_FAULT_PIN, PCFDATA[1]);

			PCFDATA[mcu] |= (1 << pin); // set back
			process_all();
			TEST_ASSERT_BITS_HIGH(LAMP_FAULT_PIN, PCFDATA[1]);
		}
	}
}

void test_end_of_charge(void)
{
	reset_all();
	shot.mode = CHARGING;
	process_all();
	TEST_ASSERT_EQUAL_UINT(CHARGING, shot.mode);

	reset_all();
	shot.mode = CHARGING;
	Uacc = 14.22;
	process_all();
	TEST_ASSERT_EQUAL_UINT(MAINTENANCE, shot.mode);

	reset_all();
	shot.mode = CHARGING;
	Ubat = 241;
	process_all();
	TEST_ASSERT_EQUAL_UINT(MAINTENANCE, shot.mode);	
}

int main(void)
{
	UNITY_BEGIN();
	RUN_TEST(test_common_state);
	RUN_TEST(test_temp);
	RUN_TEST(test_accum);
	RUN_TEST(test_bat_status);
	RUN_TEST(test_avaria);
	RUN_TEST(test_end_of_charge);

	return UNITY_END();
}
