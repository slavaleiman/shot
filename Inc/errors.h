#ifndef _ERROR_H_
#define _ERROR_H_ 1

#include "stm32f4xx.h"
#include <stdbool.h>

#define MAX_ERRORS 36

typedef enum
{
    NO_ERROR = 0,

    PCF_OUT_OF_RANGE,
    PCF_RX_ERROR,
    PCF_TX_ERROR,
    PCF_BUSY,

    TSENSOR_NO_RESPONSE,
    TSENSOR_CRC_INCORRECT,
    TSENSOR_INVALID_ADDR,

    ADC_NO_RESPONSE,

    DS_ERROR,
    DS_BUSY,
    DS_TIMEOUT,
    DS_1W_TIMEOUT,
    DS_CRC_ERROR,
    DS_SENSOR_NOT_FOUND,
    DS_NO_PRESENCE,

    SPI_RX_ERROR,
    ROM_CRC_ERROR,
    TOUCH_I2C_ERROR,
    ACC_OVERHEAT,

    ERRORS_SIZE
}error_t;

char* str_error(int32_t err);
char* errors_page(void);

void    errors_page_up(void);
void    errors_page_down(void);
void    errors_home(void);

void    errors_on_error(int err);
void    errors_init(void);

void    errors_uart_log_level(bool level);

#endif //_ERROR_H_
