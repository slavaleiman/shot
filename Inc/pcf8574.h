#ifndef __PCF_8574__
#define __PCF_8574__ 1
#include "stm32f4xx.h"
#include "errors.h"

// #define MCU_ADDR 0x20 // PCF8574
#define MCU_ADDR 0x38 // PCF8574A

#define PCF_OUTPUT_1	1
#define PCF_OUTPUT_2 	3

#define PCF_INPUT_1 	0
#define PCF_INPUT_2		2
#define PCF_INPUT_3 	4

void    pcf_init(void);
uint8_t pcf_status(void);
int8_t  pcf_configure(uint8_t mcu, uint8_t port);
void    pcf_toggle(uint8_t mcu, uint8_t pin);
void 	pcf_test_toggle(uint8_t mcu, uint8_t pin);
void    pcf_write(uint8_t mcu, uint8_t pin, uint8_t state);
void    pcf_sync(void);
error_t pcf_check(void);
int8_t  pcf_read(uint8_t mcu, uint8_t pin); // CALL ONLY AFTER pcf_check !!!
uint8_t pcf_wdata(uint8_t mcu);
uint8_t pcf_rdata(uint8_t mcu);

void pcf_exit_test(void);
void pcf_enter_test(void);

#endif
