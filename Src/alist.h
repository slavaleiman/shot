#ifndef _ALIST_H_
#define _ALIST_H_ 1
#include "stdlib.h"
#include "stm32f4xx.h"

typedef struct item_s
{
    void *data;
    struct item_s* next;
}item_t;

typedef struct alist_s alist_t;

struct alist_s
{
    uint32_t size;
    item_t *current;
    item_t *first;
    item_t *last;
};

alist_t * alist_init(void);
void alist_destroy(alist_t *list);

void alist_first(alist_t *list);
void alist_next(alist_t *list);
void alist_prev(alist_t *list); // do not uuse in long lists // optimized for item size
int alist_eol(alist_t *list);
void * alist_data(alist_t *list);
int alist_is_data(alist_t *list);
size_t alist_size(alist_t *list);
void alist_insert_head(alist_t *list, void *data);
void alist_insert_tail(alist_t *list, void *data);

void alist_remove_item(alist_t *list, void *data);

void* my_malloc(size_t size);
void my_free(void *ptr);
#endif /* _ALIST_H_ */
