#include "errors.h"
#include "shot_gui.h"
#include "dma_usart_idle.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define NUM_LINES           14
#define ERRORS_BUFFER_SIZE  32
#define ERRORS_MAX_PAGE     (ERRORS_BUFFER_SIZE / NUM_LINES)

typedef struct
{
    int      err;       // номер ошибки
    uint16_t repeated;  // количество повторов
    uint32_t last_time; // from start time
}error_item_t;

struct errors_s
{
    error_item_t    buffer[ERRORS_BUFFER_SIZE];
    uint16_t        index;
    uint16_t        page_num;
    char            page[256];
    bool            is_logging;
}errors;

    // NO_ERROR = 0,

    // PCF_OUT_OF_RANGE,
    // PCF_RX_ERROR,
    // PCF_TX_ERROR,
    // PCF_BUSY,

    // TSENSOR_NO_RESPONSE,
    // TSENSOR_CRC_INCORRECT,
    // TSENSOR_INVALID_ADDR,

    // ADC_NO_RESPONSE,

    // DS_ERROR,
    // DS_BUSY,
    // DS_TIMEOUT,
    // DS_1W_TIMEOUT,
    // DS_CRC_ERROR,
    // DS_SENSOR_NOT_FOUND,
    // DS_NO_PRESENCE,

    // SPI_RX_ERROR,
    // ROM_CRC_ERROR,
    // TOUCH_I2C_ERROR,
    // ACC_OVERHEAT,
    // ERRORS_SIZE

const char* const error_strings[] =
{
    "PCF8574 номер",
    "PCF8574 rx ошибка",
    "PCF8574 tx ошибка",
    "PCF8574 I2c занят",

    "T-датчик нет связи",
    "T-датчик ошибка CRC",
    "T-датчик ошибка адреса",

    "ADC нет связи",

    "DS2482 I2C ошибка",
    "DS2482 I2C занят",
    "DS2482 I2C таймаут",
    "1W таймаут",

    "1-Wire CRC ошибка",

    "Т-датчик не найден",
    "Т-датчик нет PPD сигнала",

    "SPI RX ошибка",
    "ROM CRC ошибка",
    "Тач.I2C ошибка",
    "Акб перегрев",
    0
};

const char* const error_strings_en[] =
{
    "PCF8574 num error",
    "PCF8574 rx error",
    "PCF8574 tx error",
    "PCF8574 I2c busy",

    "T-sensor no connect",
    "T-sensor error CRC",
    "T-sensor error addr",

    "ADC no connect",

    "DS2482 I2C error",
    "DS2482 I2C busy",
    "DS2482 I2C timeout",
    "1W timeout",

    "1-Wire CRC error",

    "Т-sensor not found",
    "Т-sensor no PPD signal",

    "SPI RX error",
    "ROM CRC error",
    "Touch I2C error",
    "Accum overheat",
    0
};

char strerr[64];

char* str_error(int32_t err)
{
    if(err >= 1)
        return (char*)error_strings[err - 1];
    else
        return "";
}

char* str_error_en(int32_t err)
{
    if(err >= 1)
        return (char*)error_strings_en[err - 1];
    else
        return "";
}

char* errors_page(void)
{
    memset(errors.page, 0, sizeof(errors.page));
    uint16_t from = errors.page_num * NUM_LINES;
    uint16_t write = 0;

    int ret = snprintf(&errors.page[write], 29, "          Page %d\n", errors.page_num + 1);
    if(ret > 0)
        write += ret;

    for(uint8_t i = 0; i < NUM_LINES; ++i) // for all strings on page
    {
        if(!errors.buffer[i].err)
            break;
        ret = snprintf(&errors.page[write], 43, "%d:%s[%d]\n",
            from + i, str_error(errors.buffer[i].err), errors.buffer[i].repeated + 1);
        if(ret > 0)
            write += ret;
        else
            break;
    }
    return errors.page;
}

void errors_page_up(void)
{
    if(errors.page_num > 0)
        --errors.page_num;
}

void errors_page_down(void)
{
    if((errors.page_num < (errors.index / NUM_LINES)) && (errors.page_num < ERRORS_MAX_PAGE))
        ++errors.page_num;
}

void errors_home(void)
{
    errors.page_num = 0;
}

void errors_on_error(int err)
{
    uint8_t index = 0;
    for(; index < ERRORS_BUFFER_SIZE; ++index)
    {
        if(err == errors.buffer[index].err)
        {
            ++errors.buffer[index].repeated;
            // TODO save last time
            break;
        }
        else if(errors.buffer[index].err == 0)
        {
            // TODO save last time
            errors.buffer[index].err = err;
            errors.index = index;
            break;
        }
    }
    if(errors.is_logging)
    {
        char buff[32];
        uint8_t sz = 0;
        sz = snprintf(buff, 32, "%s [%d]\n", str_error_en(err), errors.buffer[index].repeated);
        HAL_UART_TRANSMIT((uint8_t*)buff, sz);
    }
}

void errors_uart_log_level(bool level)
{
    errors.is_logging = level;
}

void errors_init(void)
{
    memset(errors.buffer, 0, sizeof(errors.buffer));
    errors.page_num = 0;
    errors.index = 0;
    errors.is_logging = false;
}
