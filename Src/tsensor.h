// this code made for DS18D20 temperature sensor
#ifndef _TSENSOR_H_
#define _TSENSOR_H_

#include "stm32f4xx.h"
#include <stdbool.h>
#include "errors.h"

#define TSENSOR_SKIP_ROM            0xCC
#define TSENSOR_SEARCH_ROM          0xF0
#define TSENSOR_CONVERT_T           0x44
#define TSENSOR_MATCH_ROM           0x55
#define TSENSOR_READ_SCRATCHPAD     0xBE
#define TSENSOR_WRITE_SCRATCHPAD    0x4E

#define NUM_TEMP_SENSORS    17
#define TSENSOR_MIN_VALUE   -55 * 16
#define TSENSOR_MAX_VALUE   125 * 16
#define TSENSOR_RANGE_VALUE 42
#define TSENSOR_OVR_VALUE   45

#define TSENSOR_NO_SENSOR 0xF000 // опрос не производится,
#define TSENSOR_ERROR   0xF001 // ошибка
#define TSENSOR_BUSY    0xF002 // занят
#define TSENSOT_TIMEOUT 0xF003 // таймаут

typedef struct
{
    uint32_t    active_state;                   // флаги активности датчиков
    uint8_t     addrs[NUM_TEMP_SENSORS][8];     // 8-байтовые адреса
    uint16_t    val[NUM_TEMP_SENSORS];          // значение температуры
                                                // флаги ошибок, 0xF000 - опрос не производится,
                                                //               0x0... - нет ошибки,
                                                //               0xF001 - ошибка
                                                //               0xF002 - занят
                                                //               0xF003 - таймаут
    uint8_t     rx_buffer[9];                   // sensor data
    uint8_t     rx_idx;                         // побайтовый прием
    uint8_t     tx_idx;                         // побайтовая отправка
    uint8_t     current;                        // индекс текущего датчика
    uint32_t    busy_count;

    bool        is_overrange;                   // выход за допустимый предел
    bool        is_overheat;                    // перегрев
}tsensor_t;

bool check_CRC(const uint8_t *buf, size_t bufSize);

void        tsensors_process(void);
bool        tsensors_flag(uint8_t idx);
void        tsensors_init(void);

error_t     tsensor_start_conversion(void);
error_t     tsensor_read(void);

void        tsensor_next(void);
uint8_t     tsensor_index(void);
uint16_t    tsensor_value(uint8_t idx); // get saved value

uint8_t*    tsensor_addrs(void);
void        tsensor_set_addrs(uint8_t* message); // установка адресов пачкой из памяти либо по modbus

void        tsensor_find_addr(uint8_t id);
void        tsensor_clear_addr(uint8_t id);

uint8_t*    tsensor_addr(uint8_t id);

void        tsensor_set_addr(uint8_t id, uint8_t* addr); // установка адреса
void        tsensor_set_state(uint32_t state);

void        tsensor_on_error(error_t);

void        tsensor_switch(uint8_t id, bool is_active);
bool        tsensor_is_active(uint8_t id);
uint32_t    tsensor_get_state(void);

bool        tsensor_check_temp(void);
bool        tsensor_is_overrange(void);
bool        tsensor_is_overheat(void);

bool 		tsensor_is_errors(void);
#endif
