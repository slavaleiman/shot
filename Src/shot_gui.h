#ifndef _SHOT_GUI_H_
#define _SHOT_GUI_H_

void shot_gui_init(void);

void shot_gui_update_temp(void);
void shot_gui_update_voltage(void);
void shot_gui_update(void);

#endif
