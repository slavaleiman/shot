#include <stddef.h>

#include "alist.h"
#include "soft_timer.h"

alist_t* timers = NULL;
TIM_HandleTypeDef htim2;

void soft_timer_init(void)
{
    if(!timers)
    {
        timers = alist_init();
    }
    __HAL_RCC_TIM2_CLK_ENABLE();

    uint32_t uwTimclock = HAL_RCC_GetPCLK2Freq();
    uint32_t uwPrescalerValue = (uint32_t) ((uwTimclock / 10000) - 1);

    htim2.Instance = TIM2;
    htim2.Init.Period = (10000) - 1;
    htim2.Init.Prescaler = uwPrescalerValue;
    htim2.Init.ClockDivision = 0;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    if(HAL_TIM_Base_Init(&htim2) == HAL_OK)
    {
       HAL_TIM_Base_Start_IT(&htim2);
    }

    HAL_NVIC_SetPriority(TIM2_IRQn, 6 ,0);
    HAL_NVIC_EnableIRQ(TIM2_IRQn);
}

soft_timer_t* soft_timer(uint32_t period, void (*cb)(void), bool is_one_shot, bool do_remove)
{
    if(!timers)
    {
        timers = alist_init();
        soft_timer_init();
    }

    soft_timer_t* timer = my_malloc(sizeof(soft_timer_t));
    timer->cb = cb;
    timer->one_shot = is_one_shot;
    timer->period = period + 1;
    timer->count = timer->period;
    timer->do_remove = do_remove;
    alist_insert_tail(timers, timer);
    timer->is_active = true;

    return timer;
}

void soft_timer_stop(soft_timer_t* timer)
{
    if(timer)
    {
        timer->is_active = false;
    }
}

void soft_timer_restart(soft_timer_t* timer)
{
    if(timer)
    {
        timer->count = timer->period + 1;
        timer->is_active = true;
    }
}

bool soft_timer_is_active(soft_timer_t* timer)
{
    return timer->is_active;
}

void soft_timer_tick(void)
{
    for(alist_first(timers); !alist_eol(timers); alist_next(timers))
    {
        soft_timer_t* t = alist_data(timers);
        if(t->is_active && t->count)
            --t->count;
    }
}

void soft_timer_poll(void)
{
    uint8_t toremove = 0;
    for(alist_first(timers); !alist_eol(timers); alist_next(timers))
    {
        soft_timer_t* t = alist_data(timers);
        if(t->is_active)
        {
            if(t->count == 0)
            {
                if(t->one_shot)
                {
                    t->is_active = false;
                    ++toremove;
                }else{
                    t->count = t->period + 1;
                }
                t->cb();
            }
        }
        else
        {
            ++toremove;
        }
    }
    if(toremove)
    {
        for(alist_first(timers); !alist_eol(timers); alist_next(timers))
        {
            soft_timer_t* timer = alist_data(timers);
            if(!timer->is_active && timer->do_remove)
            {
                alist_remove_item(timers, timer);
            }
        }
    }
}
