#ifndef _SOFT_TIMER_
#define _SOFT_TIMER_ 1

#include <stdbool.h>
#include "alist.h"

typedef struct
{
	uint32_t period;
    uint32_t count;
    void (*cb)(void);
    bool one_shot:1;
    bool is_active:1;
    bool do_remove:1;
}soft_timer_t;

soft_timer_t* soft_timer(uint32_t period, void (*cb)(void), bool is_one_shot, bool do_remove);
void soft_timer_stop(soft_timer_t* t);
void soft_timer_tick(void);
void soft_timer_poll(void);
void soft_timer_init(void);
void soft_timer_restart(soft_timer_t* timer);
bool soft_timer_is_active(soft_timer_t* timer);

#endif