#include "stm32f4xx.h"
#include <string.h>
#include "main.h"
#include "rom_data.h"
#include "tsensor.h"
#include "ai_control.h"
#include "CRC.h"
#include "shot.h"

// так как перезапись происходит секторами,
// и тут и там происходит запись одинаковых данных
// адреса термометров и состояние их отключенности
#define SENSORS_ADDRS_SIZE  (NUM_TEMP_SENSORS * 8)
#define SENSORS_STATE_SIZE  4
#define ADC_DATA_SIZE       40
#define CRC_SIZE            1
#define DATA_SIZE   (SENSORS_ADDRS_SIZE + SENSORS_STATE_SIZE + ADC_DATA_SIZE + CRC_SIZE)

struct{
    bool    is_changed;
    bool    is_empty;
    uint8_t data[DATA_SIZE];
    uint32_t time;
}rom;

void rom_data_changed(void)
{
    rom.is_changed = true;
}

bool rom_data_is_changed(void)
{
    if(rom.is_changed)
    {
        rom.is_changed = false;
        return true;
    }else{
        return false;
    }
}

static void write_flash(uint8_t sector, uint32_t addr, uint8_t* data, uint32_t size)
{
    HAL_FLASH_Unlock(); // unlock flash writing
    do{
        FLASH_Erase_Sector(sector, FLASH_VOLTAGE_RANGE_3);
        __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR | FLASH_SR_PGSERR);
        HAL_StatusTypeDef status = HAL_OK;
        for (uint32_t i = 0; i < size; ++i)
        {
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, addr + i, data[i]);
            if(status != HAL_OK)
                break;
        }
    }while(0);
    HAL_FLASH_Lock(); // lock the flash
}

static void write_config(uint8_t* data, uint32_t size)
{
    write_flash(DATA_SECTOR, ROM_DATA_CONFIG_ADDR, data, size);
}

void rom_write_time(void)
{
    uint8_t data[5] = {0};
    rom.time = shot_time_for_save();
    memcpy(data, &rom.time, sizeof(uint32_t));

    uint8_t crc = calc_CRC(data, 4);
    data[4] = crc;
    write_flash(TIME_SECTOR, ROM_DATA_TIME_ADDR, data, 5);
}

void read_time(void)
{
    uint8_t data[5];
    const uint8_t size = 5;

    uint32_t i;
    for(i = 0; i < size; ++i)
        data[i] = *(uint8_t*)(ROM_DATA_TIME_ADDR + i);

    memcpy(&rom.time, data, 4);

    if(!check_CRC(data, sizeof(uint32_t)))
    {
        rom.is_empty = true;
        memset(data, 0, size);
        errors_on_error(ROM_CRC_ERROR);
    }
}

static void read_flash(uint8_t* data, uint32_t size)
{
    rom.is_empty = false;
    uint32_t i;
    for(i = 0; i < size; ++i)
        data[i] = *(uint8_t*)(ROM_DATA_CONFIG_ADDR + i);

    if(!check_CRC(data, size))
    {
        rom.is_empty = true;
        memset(data, 0, size);
        errors_on_error(ROM_CRC_ERROR);
    }
}

void rom_save_data(void)
{
    if(!rom_data_is_changed())
        return;

    uint8_t data[DATA_SIZE];

    uint8_t* addrs = tsensor_addrs();

    uint32_t wr = 0;
    memcpy(&data[wr], addrs, SENSORS_ADDRS_SIZE);
    wr += SENSORS_ADDRS_SIZE;

    uint32_t state = tsensor_get_state();
    memcpy(&data[wr], &state, SENSORS_STATE_SIZE);
    wr += SENSORS_STATE_SIZE;

    uint8_t sz = ai_get_data(&data[wr]);
    wr += sz;

    uint8_t crc = calc_CRC(data, wr);
    data[wr] = crc;
    wr += 1;
    if(DATA_SIZE == wr)
        write_config(data, DATA_SIZE);
}

void rom_update_all_params(void)
{
    if(!rom.is_empty)
    {
        uint32_t rd = 0;
        tsensor_set_addrs(&rom.data[rd]);
        rd += SENSORS_ADDRS_SIZE;

        uint32_t state = (rom.data[rd + 3] << 24) | (rom.data[rd + 2] << 16) | (rom.data[rd + 1] << 8) | rom.data[rd];
        tsensor_set_state(state);
        rd += SENSORS_STATE_SIZE;

        ai_set_data(&rom.data[rd], ADC_DATA_SIZE);
        rd += ADC_DATA_SIZE;

        shot_set_start_time(rom.time);
    }else{
        ai_set_defaults();
    }
}

void rom_load_data(void)
{
    rom.is_changed = false;
    memset(rom.data, 0, DATA_SIZE);
    read_flash(rom.data, DATA_SIZE);
    read_time();
}
