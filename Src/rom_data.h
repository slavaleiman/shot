#ifndef _ROM_DATA_
#define _ROM_DATA_ 1

#include "main.h"
#include <stdbool.h>

#define OPTIONS_IN_FLASH  1
#ifdef OPTIONS_IN_FLASH
    #define TIME_SECTOR       	 	10 // FLASH_Sector_10
    #define ROM_DATA_TIME_ADDR 	 	(0x080C0000) // адрес в ROM, где хранится время
    #define DATA_SECTOR       		11 // FLASH_Sector_11
    #define ROM_DATA_CONFIG_ADDR 	(0x080E0000) // адрес в ROM, где хранятся опции опроса
#endif

void rom_load_data(void);
void rom_save_data(void);

void rom_data_changed(void);
bool rom_data_is_changed(void);

void rom_update_all_params(void);
void rom_write_time(void);

#endif //_ROM_DATA_
