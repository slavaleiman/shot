#include "stm32f4xx.h"
#include "kbd.h"

#define BUTTON_1        GPIOD, GPIO_PIN_15
#define BUTTON_2        GPIOG, GPIO_PIN_3
#define BUTTON_3        GPIOG, GPIO_PIN_2
#define BUTTON_4        GPIOD, GPIO_PIN_14

#define BUFFLEN 4
struct
{
    uint8_t kbd_input;
    uint8_t buffer[BUFFLEN];
    uint8_t pwrite;
    void (*on_button_1)(void);
    void (*on_button_2)(void);
    void (*on_button_3)(void);
    void (*on_button_4)(void);
}kbd;

void kbd_check(void)
{
    if(HAL_GPIO_ReadPin(BUTTON_1))
    {
        kbd.buffer[kbd.pwrite] &= ~KBD_BTN1_FLAG;
    }else{
        kbd.buffer[kbd.pwrite] |= KBD_BTN1_FLAG;
    }
    if(HAL_GPIO_ReadPin(BUTTON_2))
    {
        kbd.buffer[kbd.pwrite] &= ~KBD_BTN2_FLAG;
    }else{
        kbd.buffer[kbd.pwrite] |= KBD_BTN2_FLAG;
    }
    if(HAL_GPIO_ReadPin(BUTTON_3))
    {
        kbd.buffer[kbd.pwrite] &= ~KBD_BTN3_FLAG;
    }else{
        kbd.buffer[kbd.pwrite] |= KBD_BTN3_FLAG;
    }
    if(HAL_GPIO_ReadPin(BUTTON_4))
    {
        kbd.buffer[kbd.pwrite] &= ~KBD_BTN4_FLAG;
    }else{
        kbd.buffer[kbd.pwrite] |= KBD_BTN4_FLAG;
    }
    ++kbd.pwrite;
    kbd.pwrite = kbd.pwrite % BUFFLEN;

    uint8_t new_kbd_input = kbd.buffer[0];
    for(uint8_t i = 1; i < BUFFLEN; ++i)
        new_kbd_input &= kbd.buffer[i];

    if(kbd.on_button_1 && !(kbd.kbd_input & KBD_BTN1_FLAG) && (new_kbd_input & KBD_BTN1_FLAG))
        kbd.on_button_1();
    if(kbd.on_button_2 && !(kbd.kbd_input & KBD_BTN2_FLAG) && (new_kbd_input & KBD_BTN2_FLAG))
        kbd.on_button_2();
    if(kbd.on_button_3 && !(kbd.kbd_input & KBD_BTN3_FLAG) && (new_kbd_input & KBD_BTN3_FLAG))
        kbd.on_button_3();
    if(kbd.on_button_4 && !(kbd.kbd_input & KBD_BTN4_FLAG) && (new_kbd_input & KBD_BTN4_FLAG))
        kbd.on_button_4();

    kbd.kbd_input = new_kbd_input;
}

void kbd_set_cb_1(void (*on_button_1)(void))
{
    kbd.on_button_1 = on_button_1;
}

void kbd_set_cb_4(void (*on_button_4)(void))
{
    kbd.on_button_4 = on_button_4;
}

void kbd_set_cb_3(void (*on_button_3)(void))
{
    kbd.on_button_3 = on_button_3;
}

void kbd_set_cb_2(void (*on_button_2)(void))
{
    kbd.on_button_2 = on_button_2;
}

void kbd_init(void)
{
    kbd.on_button_1 = NULL;
    kbd.on_button_2 = NULL;
    kbd.on_button_3 = NULL;
    kbd.on_button_4 = NULL;
}
