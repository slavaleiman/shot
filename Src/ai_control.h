#ifndef __AI_H__
#define __AI_H__ 1

#include <stdbool.h>
#include "main.h"
#include "options.h"

#define NUM_ACCUMS                  17
#define ACCUM_FULL_VOLTAGE			14.2 	// аккум полностью заряжен
#define ACCUM_MAX_VOLTAGE           14.4	// критический уровень перезаряда
#define ACCUM_LOW_VOLTAGE           11		// аккум разряжен и скоро будет отключен
#define ACCUM_MIN_VOLTAGE           10.8 	// аккум полностью разряжен
#define ACCUM_MAX_DELTA_VOLTAGE     0.2

#define BATTERY_MAX_VOLTAGE         240

#define AI_AMPERAGE_ZERO            0.1
#define AI_AMPERAGE_5A              5.0
#define AI_VOLTAGE_181V             181.0
#define AI_VOLTAGE_183V             183.0
#define AI_VOLTAGE_190V             190.0
#define AI_VOLTAGE_204V             204.0

#define AI_ADC_MAX_NUM_CHANNELS 	21  // количество измеряемых параметров, внешних переключаемых каналов
#define AI_NUM_SAMPLES          	16  // размер буфера для сэмплов

#define VOLTAGE_BUFFER_SIZE         255
#define AMPERAGE_BUFFER_SIZE        64

#define CALIB_COEF_ZERO_VOLTAGE_DEFAULT   0x7FF
#define CALIB_COEF_ZERO_AMPERAGE_DEFAULT  0x7FF

#define CALIB_COEF_I1_DEFAULT       0.015
#define CALIB_COEF_I2_DEFAULT       0.015

#define CALIB_COEF_ACCUM_DEFAULT    0.02599
#define CALIB_COEF_COMMON_DEFAULT   0.2427

#define DISCHARGED_VOLTAGE_DEFAULT  10.8
#define CHARGE_VOLTAGE_DEFAULT      14.6
#define MINIMAL_CHARGE_AMPERAGE     1.8
#define ACCUM_CAPACITY_DEFAULT      120.0 // A*h

#define ALL_CHANNEL_ENABLE          0x1FFFFF

#ifdef MODULE_TESTING
    uint16_t OUTPUT_PORT1;
    uint16_t OUTPUT_PORT2;
    uint16_t OUTPUT_PORT3;
#else
    #define OUTPUT_PORT1 GPIOE->ODR
    #define OUTPUT_PORT2 GPIOF->ODR
    #define OUTPUT_PORT3 GPIOG->ODR
#endif

typedef struct
{
    uint8_t     channel; // текущий канал
    HAL_StatusTypeDef status;

    uint32_t    active_flag;     // флаги включенности канала, если установлен флаг - то канала пропускается при измерении
    float       accum_voltage[NUM_ACCUMS];  // среднее на каждом аккумуляторе
    float       battery_voltage; // A0 - A17
    float       output_voltage;  // A0 - A18 - напряжение нагрузки
    float       charge_amperage; // 0VI - A19 - тока заряда
    float       output_amperage; // 0VI - A20 - ток выхода

    float       battery_capacity; // A*s измеренное примерное значение емкости

    uint8_t     readbuf[2];
    uint16_t    adc_value[AI_ADC_MAX_NUM_CHANNELS][AI_NUM_SAMPLES];

    float       voltage_buffer[VOLTAGE_BUFFER_SIZE];
    uint8_t     voltage_buffer_index;
    uint8_t     voltage_buffer_fillness;

    float       amperage_buffer[AMPERAGE_BUFFER_SIZE];
    uint8_t     amperage_buffer_index;
    uint8_t     amperage_buffer_fillness;

    // опции для отображения в меню
    opt_list_t opt_list;

    option_t zero_voltage;
    option_t zero_amperage;
    option_t calib_coeff_I1;
    option_t calib_coeff_I2;
    option_t calib_coeff_accum;
    option_t calib_coeff_common;
    option_t Up;
    option_t Uz;
    option_t Izm;
    option_t Ca10;

    bool calibration;
}ai_control_t;

// commands
void ai_init(void);
void ai_start_conversion(void);
void ai_update_voltage_buffer(void);
void ai_update_amperage_buffer(void);
// get
bool  ai_status(void);
char* ai_string(void);
float ai_value_on_coeff(uint8_t opt_id);
float ai_output_voltage(void);
float ai_output_amperage(void);
float ai_battery_voltage(void);
float ai_charge_amperage(void);

float ai_mean_output_voltage(uint16_t period);
float ai_mean_charge_amperage(uint16_t period);
float ai_accum_voltage(uint8_t idx);
uint8_t ai_get_data(uint8_t* data);

opt_list_t* ai_opt_list(void);

bool ai_is_overcharge(void);
bool ai_is_lowcharge(void);
bool shot_is_leveling_charge_need(void);
// set
void ai_set_channel(uint8_t channel);
void ai_set_calibration(bool calib, int8_t opt_id);
void ai_set_poll_params(uint16_t poll_params, uint16_t poll_freq); // установка параметров опроса
void ai_set_data(uint8_t* data, uint8_t size);
void ai_set_defaults(void);

bool ai_channel_is_active(uint8_t channel);
void ai_channel_enable(uint8_t channel); // вкл/выкл
void ai_channel_disable(uint8_t channel); // вкл/выкл

float ai_Izmin(void);
void ai_battery_capacity_inc(float a);
void ai_battery_capacity_dec(float a);
float ai_battery_capacity(void);
float ai_full_capacity(void);

uint32_t ai_adc_value_summ(uint8_t channel);
ai_control_t ai_control_module(void);

#endif /*__AI_H__*/
