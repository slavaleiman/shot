#ifndef _TSENSOR_GUI_
#define _TSENSOR_GUI_ 1

#include "stm32f4xx.h"
#include <stdbool.h>

#define TSENSOR_GUI_ITEMS_ON_PAGE   12

// get
uint8_t tsensor_gui_selected_id(void);
void    tsensor_gui_update_view(void);
// commands
void tsensor_gui_draw(uint16_t x, uint16_t y);
void tsensor_gui_init(void);
void tsensor_gui_update_item(void);
void tsensor_gui_up(void);
void tsensor_gui_down(void);

#endif //_TSENSOR_GUI_
