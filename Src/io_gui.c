#include "ugui.h"
#include "io_gui.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "pcf8574.h"
#include "ili9341.h"

typedef struct
{
    int8_t  view_index; // view from
    bool    update_view;
    int8_t  old_selection;
    int8_t  selected;    // 0..IO_OUTPUTS
    uint32_t last_input_status;
}io_gui_t;

io_gui_t io_gui;

void io_gui_prev(void)
{
    if(io_gui.selected > 0)
    {
        --io_gui.selected;
        if(io_gui.selected < io_gui.view_index)
        {
            --io_gui.view_index;
            io_gui.update_view = true;
        }
    }
}

void io_gui_next(void)
{
    if(io_gui.selected < IO_OUTPUTS - 1)
    {
        ++io_gui.selected;
        if(io_gui.selected > io_gui.view_index + IO_OUTPUTS - 1)
        {
            ++io_gui.view_index;
            io_gui.update_view = true;
        }
    }
}

uint8_t io_gui_selected_id(void) // for drawing name number
{
    return io_gui.selected;
}

void io_gui_update_view(void)
{
    io_gui.update_view = true;
}

void io_gui_draw(uint16_t x, uint16_t y)
{
    const uint8_t rect_size = 20;
    const uint8_t frame_size = 21;
    char num_str[3];
    x += 10;
    if(io_gui.old_selection == -1)
        io_gui.update_view = true;

    if(io_gui.update_view)
        UG_PutString(x + 5, y, "Выходы");
    y += 18;

    const int8_t selected = io_gui.selected - io_gui.view_index;
    uint16_t output_status = pcf_wdata(PCF_OUTPUT_1) | (pcf_wdata(PCF_OUTPUT_2) << 8);
    for(int8_t i = 0; i < IO_OUTPUTS; ++i)
    {
        if(i == selected)
        {
            const uint16_t x1 = x + (i % 8) * (rect_size + 4);
            const uint8_t y1 = y + (i / 8) * (rect_size + 4);
            UG_DrawFrame(x1, y1, x1 + frame_size, y1 + frame_size, C_BLACK);
            UG_DrawFrame(x1 - 1, y1 - 1, x1 + frame_size + 1, y1 + frame_size + 1, C_BLACK);

            uint16_t color = (output_status & (1 << i)) ? C_DARK_GREEN : C_RED;
            UG_FontSelect(&FONT_8X14_RU);
            UG_SetBackcolor(color);
            UG_SetForecolor(C_WHITE);
            ILI9341_FillRectangle(x1 + 1, y1 + 1, rect_size, rect_size, color);
            sprintf(num_str, "%2d", i);
            UG_PutString(x1 + 3, y1 + 3, num_str);
            UG_SetBackcolor(UG_WindowGetBackColor());
            UG_SetForecolor(UG_WindowGetForeColor());
        }
        else if((i == io_gui.old_selection) || io_gui.update_view)
        {
            const uint16_t x1 = x + (i % 8) * (rect_size + 4); 
            const uint8_t y1 = y + (i / 8) * (rect_size + 4);
            UG_DrawFrame(x1, y1, x1 + frame_size, y1 + frame_size, UG_WindowGetBackColor());
            UG_DrawFrame(x1 - 1, y1 - 1, x1 + frame_size + 1, y1 + frame_size + 1, UG_WindowGetBackColor());
            
            uint16_t color = (output_status & (1 << i)) ? C_DARK_GREEN : C_RED;
            UG_FontSelect(&FONT_8X14_RU);
            UG_SetBackcolor(color);
            UG_SetForecolor(C_WHITE);
            ILI9341_FillRectangle(x1 + 1, y1 + 1, rect_size, rect_size, color);
            sprintf(num_str, "%2d", i);
            UG_PutString(x1 + 3, y1 + 3, num_str);
            UG_SetBackcolor(UG_WindowGetBackColor());
            UG_SetForecolor(UG_WindowGetForeColor());
        }
    }
    io_gui.old_selection = selected;

    y += 2 * rect_size + 15;
    if(io_gui.update_view)
        UG_PutString(x + 5, y, "Входы");
    y += 18;
    uint32_t input_status = pcf_rdata(PCF_INPUT_1) | (pcf_rdata(PCF_INPUT_2) << 8) | (pcf_rdata(PCF_INPUT_3) << 16);
    if(input_status != io_gui.last_input_status)
    {
        io_gui.update_view = true;
    }
    io_gui.last_input_status = input_status;

    for(int8_t i = 0; i < IO_INPUTS; ++i)
    {
        if(io_gui.update_view)
        {
            const uint16_t x1 = x + (i % 8) * (rect_size + 4); 
            const uint8_t y1 = y + (i / 8) * (rect_size + 4);
            UG_FontSelect(&FONT_8X14_RU);
            uint16_t color = (input_status & (1 << i)) ? C_RED : C_DARK_GREEN;
            UG_SetBackcolor(color);
            UG_SetForecolor(C_WHITE);
            ILI9341_FillRectangle(x1 + 1, y1 + 1, rect_size, rect_size, color);
            sprintf(num_str, "%2d", i);
            UG_PutString(x1 + 3, y1 + 3, num_str);
            UG_SetBackcolor(UG_WindowGetBackColor());
            UG_SetForecolor(UG_WindowGetForeColor());
        }
    }
    io_gui.update_view = false;
}

void io_gui_switch_state(void)
{
    uint8_t id = io_gui.selected;
    uint8_t mcu = (io_gui.selected < 8) ? PCF_OUTPUT_1 : PCF_OUTPUT_2;
    pcf_test_toggle(mcu, id % 8);
    pcf_sync();
}

void io_gui_init(void)
{
    io_gui.old_selection = -1;
    io_gui.last_input_status = 0;
    io_gui.selected = 0;
    io_gui.update_view = true;
}
