#include "pcf8574.h"
#include "stm32f4xx_hal.h"
#include <stdbool.h>
#include "FreeRTOS.h"
#include "semphr.h"

#include "task.h"
#define MCU_COUNT 5
#define PCF_TIMEOUT 200

#define HI2C    hi2c3
extern I2C_HandleTypeDef HI2C;

#define IOBUFFLEN 4
struct{
    uint8_t input_buffer[MCU_COUNT][IOBUFFLEN];   // [pin][измерения]
    uint8_t read_index;
    uint8_t io_state[MCU_COUNT];  // io state for check // вход или выход
    uint8_t rdata[MCU_COUNT];     // ports state readed // считанное значение
    uint8_t wdata[MCU_COUNT];     // ports state writen // выходной буфер
    uint8_t mcu_flag; // mcu error flags 1 - error, 0 - no errors, 0xFF - attempt to write to incorrect mcu // флаги ошибок
    uint8_t update; // mcu update flags  // MCU_COUNT MUST BE LESS THAN 8
    struct
    {
        uint8_t _or[5];
        uint8_t _and[5];
    }mask;
    bool is_test;
    uint8_t test_state[MCU_COUNT]; // нам нужны только выходы
}pcf8574;

uint8_t pcf_status(void)
{
    return pcf8574.mcu_flag;
}

static error_t i2c_transmit(uint8_t mcu, uint8_t* data, uint8_t len)
{
    error_t ret = NO_ERROR;
    taskENTER_CRITICAL();
    {
        HAL_StatusTypeDef status = HAL_I2C_Master_Transmit(&HI2C, (MCU_ADDR + mcu) << 1, data, len, PCF_TIMEOUT);
        if(HAL_OK == status)
        {
            pcf8574.mcu_flag &= ~(1 << mcu);
        }
        else if(HAL_BUSY == status)
        {
            ret = PCF_BUSY;
        }
        else // ERROR
        {
            pcf8574.mcu_flag |= 1 << mcu;
            ret = PCF_TX_ERROR;
        }
    }
    taskEXIT_CRITICAL();

    return ret;
}

static error_t i2c_receive(uint8_t mcu, uint8_t len)
{
    uint8_t *data = &pcf8574.input_buffer[mcu][pcf8574.read_index];
    pcf8574.read_index = (pcf8574.read_index + 1) % IOBUFFLEN;

    error_t ret = NO_ERROR;
    taskENTER_CRITICAL();
    {
        volatile HAL_StatusTypeDef status = HAL_I2C_Master_Receive(&HI2C, (MCU_ADDR + mcu) << 1, data, len, PCF_TIMEOUT);
        if(HAL_OK == status)
        {
            uint8_t new_input = pcf8574.input_buffer[mcu][0];
            for(uint8_t i = 1; i < IOBUFFLEN; ++i)
                new_input &= pcf8574.input_buffer[mcu][i];
            pcf8574.rdata[mcu] = new_input;
            pcf8574.mcu_flag &= ~(1 << mcu);
        }
        else if(HAL_BUSY == status)
        {
            ret = PCF_BUSY;
        }
        else // ERROR
        {
            pcf8574.rdata[mcu] = 0x00;
            pcf8574.mcu_flag |= 1 << mcu;
            ret = PCF_RX_ERROR;
        }
    }
    taskEXIT_CRITICAL();

    return ret;
}

error_t pcf_check(void) // актуальное состояние можно проверить только при чтении, так как при записи АСК может генерироавться наводками
{
    for(uint8_t mcu = 0; mcu < MCU_COUNT; ++mcu)
    {
        i2c_receive(mcu, 1);
    }
    return (pcf8574.mcu_flag) ? PCF_RX_ERROR : NO_ERROR;
}

// compare data and data_sync and write changes
void pcf_sync(void)
{
    for(uint8_t mcu = 0; mcu < MCU_COUNT; ++mcu)
    {
        if((mcu == PCF_OUTPUT_1) || (mcu == PCF_OUTPUT_2))
        {
            if(pcf8574.update & (1 << mcu))
            {
                if(pcf8574.is_test)
                {
                    i2c_transmit(mcu, &pcf8574.test_state[mcu], 1);
                }else{
                    i2c_transmit(mcu, &pcf8574.wdata[mcu], 1);
                }
            }
            pcf8574.update &= ~(1 << mcu);
        }
    }
}

// для того чтобы случайно не записать 0 во вход, прописываем маску io_state
// все входы подтягиваем к питанию
int8_t pcf_configure(uint8_t mcu, uint8_t port)
{
    if(mcu >= MCU_COUNT)
    {
        pcf8574.mcu_flag |= 0x80;
        return PCF_OUT_OF_RANGE;
    }
    pcf8574.io_state[mcu] = port;
    pcf8574.wdata[mcu] = pcf8574.io_state[mcu];
    error_t ret = i2c_transmit(mcu, &pcf8574.wdata[mcu], 1);

    return ret;
}

void pcf_write(uint8_t mcu, uint8_t pin, uint8_t state)
{
    if((mcu >= MCU_COUNT) || (pin > 7))
    {
        pcf8574.mcu_flag |= 0x80;
        return;
    }

    if(pcf8574.is_test)
        return;

    if((1 << pin) & ~pcf8574.io_state[mcu])
    {
        uint8_t* wdata = &pcf8574.wdata[mcu];
        if(state != 0)
        {
            *wdata |= (1 << pin);
        }else{
            *wdata &= ~(1 << pin);
        }
    }
    pcf8574.update |= 1 << mcu;
}

int8_t pcf_read(uint8_t mcu, uint8_t pin)
{
    if((mcu >= MCU_COUNT) || (pin > 7))
    {
        pcf8574.mcu_flag |= 0x80;
        return PCF_OUT_OF_RANGE;
    }
    if(pcf8574.mcu_flag & (1 << mcu))
        return PCF_RX_ERROR;

    if((mcu == PCF_INPUT_1)
    || (mcu == PCF_INPUT_2)
    || (mcu == PCF_INPUT_3))
        return (((pcf8574.rdata[mcu] & (1 << pin)) > 0) ? 1 : 0);
    else
        return (((pcf8574.wdata[mcu] & (1 << pin)) > 0) ? 1 : 0);
}

uint8_t pcf_wdata(uint8_t mcu)
{
    if(!pcf8574.is_test)
        return pcf8574.wdata[mcu];
    else
        return pcf8574.test_state[mcu];
}

uint8_t pcf_rdata(uint8_t mcu)
{
    return pcf8574.rdata[mcu];
}

void pcf_toggle(uint8_t mcu, uint8_t pin)
{
    if((mcu >= MCU_COUNT) || (pin > 7))
    {
        pcf8574.mcu_flag |= 0x80;
        return;
    }

    if(pcf8574.is_test)
        return;
    pcf8574.wdata[mcu] ^= (1 << pin);
    pcf8574.update |= 1 << mcu;
}

void pcf_test_toggle(uint8_t mcu, uint8_t pin)
{
    if((mcu >= MCU_COUNT) || (pin > 7))
    {
        pcf8574.mcu_flag |= 0x80;
        return;
    }

    if(!pcf8574.is_test)
        return;
    pcf8574.test_state[mcu] ^= (1 << pin);
    pcf8574.update |= 1 << mcu;
}

void pcf_exit_test(void)
{
    pcf8574.is_test = false;
    pcf_sync();
}

void pcf_enter_test(void)
{
    pcf8574.is_test = true;
    pcf8574.update |= 0x1F;
    for(uint8_t mcu = 0; mcu < MCU_COUNT; ++mcu)
    {
        if((mcu == PCF_OUTPUT_1) || (mcu == PCF_OUTPUT_2))
            pcf8574.test_state[mcu] = 0;
    }
    pcf_sync();
}

void pcf_init(void)
{
    pcf8574.mcu_flag = 0x1F; // set all mcu error flags on init
    pcf8574.read_index = 0;
}
