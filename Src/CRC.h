#ifndef _CRC_H_
#define _CRC_H_ 1

#include <stdbool.h>
#include "stm32f4xx.h"

extern const uint8_t crc_table[256];
uint8_t calc_CRC(const uint8_t *buf, size_t bufSize);
bool check_CRC(const uint8_t *buf, size_t bufSize);
#endif // _CRC_H_
