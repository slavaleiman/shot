#include "options.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <ugui.h>
#include "rom_data.h"

/*
    это такой объект ugui - список опций
    пример
    option_t opt;
    options_new_float(opt_list, &opt, "freq", "Hz", &on_change, 26.5f);
*/

option_t* get_option(opt_list_t* opt_list, uint8_t id)
{
    option_t* opt = opt_list->first;
    while(opt)
    {
        if(opt->id == id)
            return opt;
        opt = opt->next;
    }
    return NULL;
}

static void update_item(opt_list_t* opt_list, uint8_t id)
{
    option_t* opt = get_option(opt_list, id);
    if(!opt)
        return;
    switch(opt->type)
    {
        case _float:
            if(opt->is_coeff)
            {
                sprintf(opt->str_value, "%.4f ", opt->val.fval);
            }else{
                sprintf(opt->str_value, "%.1f %s", opt->val.fval, opt->units);
            }
            break;
        case _int:
            if(opt->is_coeff)
            {
                sprintf(opt->str_value, "%d ", opt->val.ival);
            }else{
                sprintf(opt->str_value, "%d %s", opt->val.ival, opt->units);
            }
            break;
    }
}

void options_update_item(opt_list_t* opt_list)
{
    update_item(opt_list, opt_list->selected);
}

void options_update(opt_list_t* opt_list)
{
    for(uint8_t i = 0; i < opt_list->last->id + 1; ++i)
    {
        update_item(opt_list, i);
    }
}

char* options_item_value(opt_list_t* opt_list, uint8_t str_num) // str_num = 0..TSENSOR_GUI_ITEMS_ON_PAGE
{
    uint8_t id = opt_list->view_index + str_num;
    option_t* opt = get_option(opt_list, id);
    return opt->str_value;
}

uint8_t options_item_id(opt_list_t* opt_list, uint8_t str_num)
{
    uint8_t id = opt_list->view_index + str_num;
    return id;
}

char* options_item_name(opt_list_t* opt_list, uint8_t str_num)
{
    uint8_t id = opt_list->view_index + str_num;
    option_t* opt = get_option(opt_list, id);
    return opt->name;
}

bool options_item_is_coeff(opt_list_t* opt_list, uint8_t str_num)
{
    uint8_t id = opt_list->view_index + str_num;
    option_t* opt = get_option(opt_list, id);
    return opt->is_coeff;
}

char* options_item_units(opt_list_t* opt_list, uint8_t str_num)
{
    uint8_t id = opt_list->view_index + str_num;
    option_t* opt = get_option(opt_list, id);
    return opt->units;
}

void options_new_float(opt_list_t* opt_list, option_t* opt, const char* name, const char* units, float (*cb)(option_t*, int8_t), bool is_coeff)
{
    opt->type = _float;
    opt->next = NULL;
    opt->is_coeff = is_coeff;
    opt->cb.fcb = cb;
    strcpy(opt->name, name);
    strcpy(opt->units, units);
    if(!opt_list->first)
    {
        opt_list->first = opt;
        opt->id = 0;
    }
    if(opt_list->last)
    {
        opt_list->last->next = opt;
        opt->id = opt_list->last->id + 1;
    }
    opt_list->last = opt;
    opt->val.fval = opt->cb.fcb(opt, 0); // set default value
}

void options_new_int(opt_list_t* opt_list, option_t* opt, const char* name, const char* units, int (*cb)(option_t*, int8_t), bool is_coeff)
{
    opt->type = _int;
    opt->next = NULL;
    opt->is_coeff = is_coeff;
    opt->cb.icb = cb;
    strcpy(opt->name, name);
    strcpy(opt->units, units);
    if(!opt_list->first)
    {
        opt_list->first = opt;
        opt->id = 0;
    }
    if(opt_list->last)
    {
        opt_list->last->next = opt;
        opt->id = opt_list->last->id + 1;
    }
    opt_list->last = opt;
    opt->val.ival = opt->cb.icb(opt, 0); // set default value
}

void options_inc(opt_list_t* opt_list)
{
    uint8_t id = opt_list->selected;
    option_t* opt = get_option(opt_list, id);
    if(opt && opt->cb.icb) // here we don't care icb or fcb
    {
        switch(opt->type)
        {
            case _float:
                opt->val.fval = opt->cb.fcb(opt, +1); // may be make shift for step change
                break;
            case _int:
                opt->val.ival = opt->cb.icb(opt, +1);
                break;
        }
    }
    rom_data_changed();
}

void options_dec(opt_list_t* opt_list)
{
    uint8_t id = opt_list->selected;
    option_t* opt = get_option(opt_list, id);
    if(opt && opt->cb.icb) // here we don't care icb or fcb
    {
        switch(opt->type)
        {
            case _float:
                opt->val.fval = opt->cb.fcb(opt, -1); // may be make shift for step change
                break;
            case _int:
                opt->val.ival = opt->cb.icb(opt, -1);
                break;
        }
    }
    rom_data_changed();
}

void options_down(opt_list_t* opt_list)
{
    if(opt_list->selected < opt_list->last->id)
    {
        ++opt_list->selected;
        if(opt_list->selected > opt_list->view_index + OPTIONS_ITEMS_ON_PAGE - 1)
        {
            ++opt_list->view_index;
            opt_list->update_view = true;
        }
    }
}

void options_up(opt_list_t* opt_list)
{
    if(opt_list->selected > 0)
    {
        --opt_list->selected;
        if(opt_list->selected < opt_list->view_index)
        {
            --opt_list->view_index;
            opt_list->update_view = true;
        }
    }
}

void options_update_view(opt_list_t* opt_list)
{
    opt_list->update_view = true;
}

void options_edit(opt_list_t* opt_list, bool is_edit)
{
    opt_list->is_edit = is_edit;
}

bool options_is_edit(opt_list_t* opt_list)
{
    return opt_list->is_edit;
}

uint8_t options_selected_id(opt_list_t* opt_list)
{
    return opt_list->selected;
}

void options_init(opt_list_t* opt_list)
{
    options_update(opt_list);
    opt_list->old_selection = -1;
    opt_list->selected = 0;
    opt_list->update_view = true;
    opt_list->is_edit = false;
}
