#include "stm32f4xx.h"
#include "stm32f4xx_hal_uart.h"
#include "string.h"
#include "dma_usart_idle.h"

// pcf_mask(port)           // get mask
// pcf_mask_and(port, mask) // set mask
// pcf_mask_or(port, mask)  // set mask
// set_io(port, pin)
// shot_status()

#define DMA_RX_BUFFER_SIZE          255
extern uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];

#define UART_BUFFER_SIZE            255
extern uint8_t UART_Buffer[UART_BUFFER_SIZE];

#define USR_BUF_SIZE 255
struct
{
    char rx_buffer[USR_BUF_SIZE];
    char tx_buffer[USR_BUF_SIZE];
}usrcmd;

#include <stdio.h>
#include <stdlib.h>
#include "pcf8574.h"

const char* help_msg = "\
    help    - show this message\n\
    log     - turn on/off uart logging\n\
        <state=[0..1]>\n\
          0 - not logging\n\
          1 - errors only\n\
    set_io  - set pcf8574 pin io mask\n\
        <mcu=[0..4]>\n\
        <pin=[0..7]>\n\
        <state=[0..1]>\n\
";

void user_cmd(uint8_t* message, uint8_t size)
{
    char* help = strstr((char*)message, "help");
    if(help)
    {
        uint8_t sz = sprintf(usrcmd.tx_buffer, help_msg);
        HAL_UART_TRANSMIT((uint8_t*)usrcmd.tx_buffer, sz);
        return;
    }

    char* log_msg = strstr((char*)message, "log");
    if(log_msg)
    {
        char* eptr;
        char* from = log_msg + 3; //strlen(log_msg);
        int log_level = strtol(from, &eptr, 10);
        if(eptr == from)
            return;

        errors_uart_log_level(log_level);

        uint8_t sz = sprintf(usrcmd.tx_buffer, "OK");
        HAL_UART_TRANSMIT((uint8_t*)usrcmd.tx_buffer, sz);
        return;
    }

    char* found = strstr((char*)message, "set_io");
    if(found)
    {
        char* eptr;
        int mcu = strtol(found, &eptr, 10);
        int pin = strtol(eptr, &eptr, 10);
        int state = strtol(eptr, &eptr, 10);

        if(!mcu || !pin || !state)
            return;

        pcf_write(mcu, pin, state);
        pcf_sync();
        uint8_t sz = sprintf(usrcmd.tx_buffer, "OK");
        HAL_UART_TRANSMIT((uint8_t*)usrcmd.tx_buffer, sz);
    }
}
