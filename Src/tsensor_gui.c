#include "ugui.h"
#include "tsensor_gui.h"
#include "tsensor.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

typedef struct
{
    char item_string[64]; // строка в которой отражен номер адрес и состояние термо датчика
    // русские буквы в utf-8 занимают по 2 байта
}tsensor_gui_item_t;

typedef struct
{
    int8_t  view_index; // view from
    bool    update_view;
    int8_t  old_selection;
    int8_t  selected;    // 0..TSENSOR_GUI_ITEMS_ON_PAGE
    tsensor_gui_item_t item[NUM_TEMP_SENSORS];
}tsensor_gui_t;

tsensor_gui_t tsensor_gui;

static void update_item(uint8_t id)
{
    uint8_t* addr = tsensor_addr(id);
    if(addr[0] == 0x28)
    {
        sprintf(tsensor_gui.item[id].item_string, "%2d:%02x%02x%02x%02x%02x%02x%02x%02x: %s",
            id + 1, addr[0], addr[1], addr[2], addr[3], addr[4], addr[5], addr[6], addr[7], (tsensor_is_active(id)) ? "Вкл ": "Выкл");
    }else{
        sprintf(tsensor_gui.item[id].item_string, "%2d:Откл                   ", id + 1);
    }
}

void tsensor_gui_update_item(void)
{
    update_item(tsensor_gui.selected);
}

static void tsensor_gui_update(void)
{
    for(uint8_t i = 0; i < NUM_TEMP_SENSORS; ++i)
    {
        update_item(i);
    }
}

static char* item_string(uint8_t str_num) // str_num = 0..TSENSOR_GUI_ITEMS_ON_PAGE
{
    uint8_t id = (tsensor_gui.view_index + str_num) % NUM_TEMP_SENSORS;
    return tsensor_gui.item[id].item_string;
}

void tsensor_gui_up(void)
{
    if(tsensor_gui.selected > 0)
    {
        --tsensor_gui.selected;
        if(tsensor_gui.selected < tsensor_gui.view_index)
        {
            --tsensor_gui.view_index;
            tsensor_gui.update_view = true;
        }
    }
}

void tsensor_gui_down(void)
{
    if(tsensor_gui.selected < NUM_TEMP_SENSORS - 1)
    {
        ++tsensor_gui.selected;
        if(tsensor_gui.selected > tsensor_gui.view_index + TSENSOR_GUI_ITEMS_ON_PAGE - 1)
        {
            ++tsensor_gui.view_index;
            tsensor_gui.update_view = true;
        }
    }
}

uint8_t tsensor_gui_selected_id(void) // for drawing name number
{
    return tsensor_gui.selected;
}

void tsensor_gui_update_view(void)
{
    tsensor_gui.update_view = true;
}

void tsensor_gui_draw(uint16_t x, uint16_t y)
{
    if(tsensor_gui.old_selection == -1)
        tsensor_gui.update_view = true;

    const int8_t selected = tsensor_gui.selected - tsensor_gui.view_index;

    for(int8_t i = 0; i < TSENSOR_GUI_ITEMS_ON_PAGE; ++i)
    {
        if(i == selected)
        {
            UG_FontSelect(&FONT_8X14_RU);
            UG_SetBackcolor(C_DEEP_SKY_BLUE);
            UG_PutString(x, y, item_string(i));
            UG_SetBackcolor(UG_WindowGetBackColor());
        }
        else if((i == tsensor_gui.old_selection) || tsensor_gui.update_view)
        {
            UG_FontSelect(&FONT_8X14_RU);
            UG_PutString(x, y, item_string(i));
        }
        y += 17;
    }
    tsensor_gui.old_selection = selected;
    tsensor_gui.update_view = false;
}

void tsensor_gui_init(void)
{
    tsensor_gui_update();
    tsensor_gui.old_selection = -1;
    tsensor_gui.selected = 0;
    tsensor_gui.update_view = true;
}
