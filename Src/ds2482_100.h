#ifndef __DS2482_100__
#define __DS2482_100__
#include <stdbool.h>
#include "errors.h"

// status register bits
#define DS_STATUS_1WB   0x01  // 1-Wire Busy
#define DS_STATUS_PPD   0x02  // Presence-Pulse Detect
#define DS_STATUS_SD    0x04  // Short Detected
#define DS_STATUS_LL    0x08  // Logic Level
#define DS_STATUS_RST   0x10  // Device Reset
#define DS_STATUS_SBR   0x20  // Single Bit Result
#define DS_STATUS_TSB   0x40  // Triplet Second Bit
#define DS_STATUS_DIR   0x80  // Branch Direction Taken

bool ds2482_busy(void);

error_t  ds2482_init(void);
error_t  ds2482_write_config(void); // after this do check status of ds2482
error_t  ds2482_reset(void);
error_t  ds2482_is_error(void);
// 1wire
error_t ds2482_1w_reset(void);
error_t ds2482_1w_write_byte(uint8_t sendbyte);
error_t ds2482_1w_read_byte(uint8_t* byte);
error_t ds2482_1w_write_triplet(uint8_t config);

error_t ds2482_1w_search(void);
uint8_t* ds2482_rom(void);

#endif // __DS2482_100__

// #define DS2482_STATUS_REGISTER         0xF0
// #define DS2482_READ_DATA_REGISTER      0xE1
// #define DS2482_CONFIGURATION_REGISTER  0xC3
