#ifndef ___UPS_CONTROL_H___
#define ___UPS_CONTROL_H___ 1

//  oooooooo8   ooooooo  oooo   oooo ooooooooooo ooooo  ooooooo8
// o888     88 o888   888o 8888o  88   888    88   888 o888    88
// 888         888     888 88 888o88   888ooo8     888 888    oooo
// 888o     oo 888o   o888 88   8888   888         888 888o    88
//  888oooo88    88ooo88  o88o    88  o888o       o888o 888ooo888

#include <stdbool.h>
#include"soft_timer.h"

#define BATTERY_CHECK_PAUSE         16000   // msec
#define BATTERY_CHECK_PERIOD        132000 	// msec
typedef enum
{
	BAT_NOT_CONNECTED,  // Батарея не включилась 	DO15=вкл && DI20=1 				белая надпись на красном фоне
	BAT_SWITCHED_OFF, 	// Батарея отключена	 	DO15=выкл						белая надпись на красном фоне
	BAT_BROKEN,			// Батарея оборвана			DO15=1 && Iзаряда=[-0.5..+0.5]B белая надпись на красном фоне
	BAT_LOW,			// Батарея разряжена 		U < 183 В, 						красная надпись на жёлтом фоне
	BAT_DISCHARGING,	// Батарея разряжается		Iзаряда<-0.1A, 					красная надпись на белом фоне
	BAT_CHARGING,		// Батарея заряжается		режим заряда					чёрная надпись на жёлтом фоне
	BAT_MAINTENANCE, 	// Режим сохранения 		режим сохранения				чёрная надпись на белом фоне
	BAT_CHECK,			// Проверка батареи 		режим проверки					чёрная надпись на белом фоне
	BAT_INIT
}battery_status_t;

typedef enum
{
    INIT_MODE = 0,
    MAINTENANCE,        // содержание
    CHARGING,           // заряд
    CHECK_BATTERY,      // проверка
}charge_mode_t;

// shot control_status
#define INSUL_CONTROL_ENABLED_FLAG  	(1 << 0) // контроль изоляции включен?
#define INSUL_CONTROL_FLAG          	(1 << 1) // контроль изоляции не в норме
#define CIRCUIT_BREAKERS_U_FLAG     	(1 << 2) // Сработала защита по напряжению
#define CIRCUIT_BREAKERS_I_FLAG     	(1 << 3) // Сработала защита по току
#define CHARGER1_FLAG               	(1 << 4) // источник питания 1 не в норме
#define CHARGER2_FLAG               	(1 << 5) // источник питания 2 не в норме
#define CHARGER3_FLAG               	(1 << 6) // источник питания 3 не в норме
#define FEEDER1_FLAG                    (1 << 7) // фидер 1 не в норме
#define FEEDER2_FLAG                    (1 << 8) // фидер 2 не в норме
#define FEEDER3_FLAG                    (1 << 9) // фидер 3 не в норме
#define LAMP_ACCUM_BAT_BLINK_FLAG		(1 << 10) // мигающий сигнал батареи

// shot measure status
#define CRITICAL_LOW_VOLTAGE_FLAG 	    (1 << 0) // критически низкое напряжение на батарее, не путать с выходным сигналом понижения напряжения заряда для источников
#define BATTERY_LOW_VOLTAGE_FLAG        (1 << 1) // напряжение ниже 205В, требуется зарядка
#define BATTERY_LOW_AMPERAGE_FLAG       (1 << 2) // низкий ток на батарее, при проверке
#define ACCUM_OVERCHARGE_FLAG       	(1 << 3) // высокое напряжение на аккумуляторе
#define ACCUM_DIFFERENT_CHARGE_FLAG 	(1 << 4) // недопустимый разброс напряжений на аккумуляторах
#define BATTERY_DAMAGED_FLAG			(1 << 5) // акуумулятор не в порядке
#define TEMP_SENSOR_WARNING_FLAG		(1 << 6) // температура выходит за диапазон
#define TEMP_SENSOR_ERROR_FLAG 			(1 << 7) // не получилось измерить температуру, либо температура превысила критический уровень
#define LOW_OUTPUT_VOLTAGE_FLAG  		(1 << 8) // напряжение выхода ниже 190В
#define DISCHARGING_FLAG 		    	(1 << 9) // ток ниже нуля
#define ACCUM_LOW_VOLTAGE_FLAG          (1 << 10) // напряжение на одном из аккумов низкое
#define ACCUM_FULL_CHARGED_FLAG         (1 << 11) // 1 из акуумуляторов заряжен
typedef struct
{
    uint32_t start_time;
    uint32_t count;
    uint32_t count_ovr;

    charge_mode_t     mode;
    bool mode_switch;
    charge_mode_t pending_mode;

    // counters
    soft_timer_t* check_voltage_timer;

    uint16_t control_status;    // состояние автоматов
    uint16_t measure_status;    // статус измерений температуры, напряжений и тока

    // флаги перезаряда банок для отображения в меню ошибок перезаряда
    // этот флаг не сбрасывается при снижении напряжения до нормы
    uint32_t current_accum_overcharge;  // если в течении 8 секунд, статус не пропадет
    uint32_t accum_overcharge;          // то он сохраняется в основном статусе

    struct
    {
        uint64_t            level;      // уровень заряда %
        uint64_t            max_level;  // максимальный заряд
        battery_status_t    status;     // суммарный статус батареи для вывода на экран
    }battery;
    float max_delta_voltage;

#ifdef SLEEP_ENABLED
    struct
    {
        bool     is_sleep;
        uint16_t sleep_counter;
    }lcd;
#endif
    int32_t error;
    uint8_t feeder_priority; // 0 - равный приоритет
                             // 1 - приоритет первого
                             // 2 - приоритет второго
                             // 3 - приоритет третьего
                             // 4 - force 1 prio
                             // 5 - force 2 prio
                             // 6 - force 3 prio
}shot_t;

void shot_init(void);
void shot_check(void);
void shot_start_adc(void);
void shot_ready_blink(void);
void shot_process_input(void);
void shot_process_output(void);
void process_mode_button(void);
void shot_process_monitor(void);
void shot_check_temp(void);
void shot_check_battery(void);
bool shot_can_check_battery(void);

uint8_t shot_control_status(void);
uint8_t shot_measure_status(void);
bool shot_is_error(uint8_t flag);
// get
void shot_operation_time(char* string);
void shot_battery_check_enable(void);
void shot_battery_check_disable(void);

uint32_t shot_battery_level(void);
battery_status_t shot_battery_status(void);

uint32_t shot_time_to_full_charge(void);
uint32_t shot_time_to_power_off(void);

bool shot_is_charge(void);
void shot_check_accum_voltage(void);

void shot_process_mode(void);
uint32_t shot_accum_overcharge(void);

uint8_t shot_feeder_priority(void);
charge_mode_t shot_mode(void);
uint32_t shot_time_for_save(void);
void shot_set_start_time(uint32_t data);
void shot_process_status(void);

void shot_battery_status_update(void);

// USART3
#define PIN_USART_TX                        GPIOB, GPIO_PIN_10  // PB10
#define PIN_USART_RX                        GPIOB, GPIO_PIN_11  // PB11

// ooooo   ooooooo     oooooooo8      ooooo        o88  ooooooo
//  888  o88     888 o888     88       888        o88 o888   888o
//  888        o888  888               888      o88   888     888
//  888     o888   o 888o     oo       888    o88     888o   o888
// o888o o8888oooo88  888oooo88       o888o o88         88ooo88

// extern I2C_HandleTypeDef hi2c3;
// I2C3
// SCL - PA8
// SDA - PC9

#define PIN_RESET   0
#define PIN_SET     1

// перед каждым READ_PIN всегда должен быть INPUT_CHECK, READ - чтение из

#define INPUT_CHECK pcf_check   // read all mcu pin, update data buffer
#define READ_PIN    pcf_read    // call only after check
#define WRITE_PIN   pcf_write
#define TOGGLE_PIN  pcf_toggle
#define SYNC_PORTS  pcf_sync    // write changes to mcu

// ooooo oooo   oooo oooooooooo ooooo  oooo ooooooooooo  oooooooo8
//  888   8888o  88   888    888 888    88  88  888  88 888
//  888   88 888o88   888oooo88  888    88      888      888oooooo
//  888   88   8888   888        888    88      888             888
// o888o o88o    88  o888o        888oo88      o888o    o88oooo888

#define PCF_INPUTS ((1 << 0) | (1 << 2) | (1 << 4))

//                                    MCU addr, PORT
// MCU0
// Авария ввода 1
#define PCFIN_FAULT_INPUT_1                0, 0	// DI00
// Напряжение ввода 1
#define PCFIN_VOLTAGE_INPUT_1              0, 1
// Включение ввода 1
#define PCFIN_ENABLE_INPUT_1               0, 2
// Авария ввода 2
#define PCFIN_FAULT_INPUT_2                0, 3
// Напряжение ввода 2
#define PCFIN_VOLTAGE_INPUT_2              0, 4
// Включение ввода 2
#define PCFIN_ENABLE_INPUT_2               0, 5
// Авария ввода 3 // резерв
#define PCFIN_FAULT_INPUT_3                0, 6 // not used in code now
// Напряжение ввода 3
#define PCFIN_VOLTAGE_INPUT_3              0, 7 // not used in code now

// MCU2
// Включение ввода 3
#define PCFIN_ENABLE_INPUT_3               2, 0 // not used in code now
// Принудительное вкл. ввода 1
#define PCFIN_FORCE_INPUT_1                2, 1	// DI09
// Принудительное вкл. ввода 2
#define PCFIN_FORCE_INPUT_2                2, 2
// Приоритет выбора ввода 1
#define PCFIN_PRIORITY_INPUT_1             2, 3
// Приоритет выбора ввода 2
#define PCFIN_PRIORITY_INPUT_2             2, 4
// Сработала защита по напряжению
#define PCFIN_VOLTAGE_FAULT                2, 5 // DI13
// Сработала защита по току
#define PCFIN_AMPERAGE_FAULT               2, 6
// Сработала защита по КИ
#define PCFIN_INSULATION_FAULT             2, 7
// MCU4
// Отключение звонка и КИ
#define PCFIN_INSULATION_AND_RING_ENABLE   4, 0 // DI16
// Исправность источника 1 - 220В
#define PCFIN_CHARGER_STATUS_1             4, 1
// Исправность источника 2
#define PCFIN_CHARGER_STATUS_2             4, 2
// Исправность источника 3
#define PCFIN_CHARGER_STATUS_3             4, 3

// Батарея включилась
#define PCFIN_BATTERY_ENABLED              4, 4 // DI20
// кнопка режим заряда
#define PCFIN_MODE_BUTTON                  4, 5

//   ooooooo  ooooo  oooo ooooooooooo oooooooooo ooooo  oooo ooooooooooo  oooooooo8
// o888   888o 888    88  88  888  88  888    888 888    88  88  888  88 888
// 888     888 888    88      888      888oooo88  888    88      888      888oooooo
// 888o   o888 888    88      888      888        888    88      888             888
//   88ooo88    888oo88      o888o    o888o        888oo88      o888o    o88oooo888
// MCU1
// включение повышенного напряжения заряда 240 В
#define PCFOUT_HIGH_VOLTAGE_ENABLE          1, 0	// DO00
// включение пониженного напряжения заряда 183 В
#define PCFOUT_LOW_VOLTAGE_ENABLE           1, 1	// DO01
// лампа предупреждение
#define LAMP_WARNING      					1, 2 	// DO02
// лампа авария
#define LAMP_FAULT               			1, 3	// DO03
// есть предупреждение или авария
#define RING_THE_ALARM               		1, 4 	// DO04
// индикатор отключения звуковой сигнализации контроля изоляции
#define LAMP_INSULATION_CONTROL_DISABLED    1, 5    // - если КИ выключена - лампа горит
// питание нагрузки от аккумуляторов
#define LAMP_ACCUM_BAT_SUPPLY_ENABLED       1, 6
// работа контроллера - норма
#define LAMP_CONTROLLER_READY               1, 7

// MCU3
// подключение батарей
#define PCF_BATTERY_CONNECT              	3, 7

#endif /*___UPS_CONTROL_H___*/
