// опрос датчиков в блокирующем режиме
#include <string.h>
#include "tsensor.h"
#include "rom_data.h"
#include "stm32f4xx.h"
#include "ds2482_100.h"
#include "errors.h"
#include "cmsis_os.h"
#include "shot_gui.h"

// все датчики начинают измерение одновременно
// чтение измеренных значений происходит по-отдельности

tsensor_t tsensor;

static int8_t on_error(int8_t err)
{
    errors_on_error(err);
    return err;
}

void tsensor_set_state(uint32_t state)
{
    tsensor.active_state = state;
}

uint32_t tsensor_get_state(void)
{
    return tsensor.active_state;
}

bool tsensor_is_active(uint8_t id)
{
    if(id < NUM_TEMP_SENSORS)
        return (bool)(tsensor.active_state & (1 << id));
    return false;
}

void tsensor_switch(uint8_t id, bool is_active)
{
    if(id < NUM_TEMP_SENSORS)
    {
        if(is_active)
            tsensor.active_state |= 1 << id;
        else
            tsensor.active_state &= ~(1 << id);
    }
    rom_data_changed();
}

void tsensor_on_error(error_t error)
{
    tsensor.val[tsensor.current] = 0xF000 | error;
    errors_on_error(error);
}

uint8_t* tsensor_addrs(void)
{
    return tsensor.addrs[0];
}

// для начала измерения температуры всех термодатчиков используется SKIP ROM
// --------------------------------------------------------------------------
//     TX Reset Reset pulse (480-960 μs).
//     RX Presence Presence pulse.
//     TX CCh Issue “Skip ROM” command.
//     TX 44h Issue “Convert T” command.
//     TX <I/O LINE HIGH> I/O line is held high for at least a period of time greater
//         than tconv by bus master to allow conversion to complete.

//  oooooooo8 ooooooooooo   o      oooooooooo  ooooooooooo        oooooooo8   ooooooo  oooo   oooo ooooo  oooo ooooooooooo oooooooooo  ooooooooooo
// 888        88  888  88  888      888    888 88  888  88      o888     88 o888   888o 8888o  88   888    88   888    88   888    888 88  888  88
//  888oooooo     888     8  88     888oooo88      888          888         888     888 88 888o88    888  88    888ooo8     888oooo88      888
//         888    888    8oooo88    888  88o       888          888o     oo 888o   o888 88   8888     88888     888    oo   888  88o       888
// o88oooo888    o888o o88o  o888o o888o  88o8    o888o          888oooo88    88ooo88  o88o    88      888     o888ooo8888 o888o  88o8    o888o

// инициирует измерение температуры на всех датчиках
// еще чтение их значений при отсутствии ошибок
error_t tsensor_start_conversion(void)
{
    error_t ret = ds2482_1w_reset();
    if(ret) // error
        return ret;

    ret = ds2482_1w_write_byte(TSENSOR_SKIP_ROM);
    if(ret)
        return ret;

    ret = ds2482_1w_write_byte(TSENSOR_CONVERT_T);
    if(ret)
        return ret;

    return NO_ERROR;
}

static error_t set_config(void)
{
    error_t ret = ds2482_1w_reset();
    if(ret)
        return ret;

    ret = ds2482_1w_write_byte(TSENSOR_WRITE_SCRATCHPAD);
    if(ret)
        return ret;

    // write config
    // set 9 bit resolution
    // T-high
    ret = ds2482_1w_write_byte(0x1F);
    if(ret)
        return ret;
    // send low
    ret = ds2482_1w_write_byte(0);
    if(ret)
        return ret;

    return NO_ERROR;
}

uint8_t tsensor_index(void)
{
    return tsensor.current;
}

// oooooooooo  ooooooooooo      o      ooooooooo
//  888    888  888    88      888      888    88o
//  888oooo88   888ooo8       8  88     888    888
//  888  88o    888    oo    8oooo88    888    888
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88

// для чтения используется MATCH ROM
// ---------------------------------
//     TX Reset Reset pulse.
//     RX Presence Presence pulse.
//     TX 55h Issue “Match ROM” command.
//     TX <64-bit ROM code> Issue address for DS18B20.
//     TX BEh Issue “Read Scratchpad” command.
//     RX <9 data bytes> Read entire scratchpad plus CRC; the master now
//         recalculates the CRC of the eight data bytes received
//         from the scratchpad, compares the CRC calculated and
//         the CRC read. If they match, the master continues; if
//         not, this read operation is repeated.
//     TX Reset Reset pulse.
//     RX Presence Presence pulse, done.
error_t tsensor_read(void)
{
    if(tsensor.addrs[tsensor.current][0] != 0x28) // check family code
    {
        tsensor.active_state &= ~(1 << tsensor.current);
        tsensor.val[tsensor.current] = 0xF000 | TSENSOR_INVALID_ADDR;
        return TSENSOR_INVALID_ADDR;
    }

    error_t ret = ds2482_1w_reset();
    if(ret){
        tsensor.val[tsensor.current] |= 0xF000 & TSENSOR_NO_RESPONSE;
        return ret;
    }

    ret = ds2482_1w_write_byte(TSENSOR_MATCH_ROM);
    if(ret){
        tsensor.val[tsensor.current] |= 0xF000 & TSENSOR_NO_RESPONSE;
        return ret;
    }

    for(uint8_t i = 0; i < 8; ++i)
    {
        ret = ds2482_1w_write_byte(tsensor.addrs[tsensor.current][i]);
        if(ret)
        {
            tsensor.val[tsensor.current] |= 0xF000 & TSENSOR_NO_RESPONSE;
            return ret;
        }
    }

    ret = ds2482_1w_write_byte(TSENSOR_READ_SCRATCHPAD);
    if(ret){
        tsensor.val[tsensor.current] |= 0xF000 & TSENSOR_NO_RESPONSE;
        return ret;
    }

    uint8_t buffer[9];  // we need to read all 9 byte
    for(uint8_t i = 0; i < 9; ++i)
    {
        ret = ds2482_1w_read_byte(&buffer[i]);
        if(ret)
        {
            tsensor.val[tsensor.current] |= 0xF000 & TSENSOR_NO_RESPONSE;
            return ret;
        }
    }

    tsensor.val[tsensor.current] = buffer[0] | (buffer[1] << 8);

    return NO_ERROR;
}

// get saved value from buffer
uint16_t tsensor_value(uint8_t idx)
{
    return tsensor.val[idx];
}

void tsensors_process(void)
{
    error_t ret;
    if(tsensor_index() == 0)
    {
        ret = tsensor_start_conversion();
        if(ret)
        {
            tsensor.val[tsensor.current] |= 0xF000 & TSENSOR_NO_RESPONSE;
            shot_gui_update_temp();
            on_error(ret);
            return;
        }
    }

    do
    {
        if(tsensor.active_state & (1 << tsensor.current))
        {
            ret = tsensor_read();
            if(ret)
                on_error(ret);
        }

        if(tsensor.current < NUM_TEMP_SENSORS)
            ++tsensor.current;
        else
            tsensor.current = 0;

    }while(tsensor.current);

    shot_gui_update_temp();
}

uint16_t compare_buffers(uint8_t* buf1, uint8_t* buf2, uint16_t size)
{
    for(uint16_t i = 0; i < size; ++i)
    {
        if(buf1[i] != buf2[i])
            return i + 1;
    }
    return 0;
}

bool is_addr_exist(uint8_t* addr)
{
    for(uint8_t i = 0; i < NUM_TEMP_SENSORS; ++i)
    {
        if(compare_buffers(&tsensor.addrs[i][0], addr, 8) == 0)
            return true;
    }
    return false;
}

void tsensor_find_addr(uint8_t id)
{
    // TODO найти первый попавшийся на шине датчик
    error_t ret = NO_ERROR;
    // при поиске пропускаются уже сохраненные адреса
    // поиск останавливается после того как был найден один сенсор

    uint8_t* addr = NULL;
    for(uint8_t i = 0; i < NUM_TEMP_SENSORS; ++i)
    {
        ret = ds2482_1w_search();
        if(ret)
        {
            errors_on_error(ret);
            return;
        }
        addr = ds2482_rom();

        if(addr[0] != 0x28) // not valid addr
            return;

        if(!is_addr_exist(addr)) // если адрес уже используется, запускаем новый цикл поиска
            break;
    }
    memcpy(tsensor.addrs[id], addr, 8);
    // сохранить адрес, если найден
    rom_data_changed();
}

void tsensor_clear_addr(uint8_t id)
{
    memset(tsensor.addrs[id], 0, 8);
    rom_data_changed();
}

void tsensor_set_addr(uint8_t id, uint8_t* addr) // установка адреса
{
    memcpy(tsensor.addrs[id], addr, 8);
    rom_data_changed();
}

void tsensor_set_addrs(uint8_t* message)
{
    uint8_t* addrs = tsensor_addrs();
    if(compare_buffers(message, addrs, NUM_TEMP_SENSORS * 8))
    {
        memcpy(addrs, message, NUM_TEMP_SENSORS * 8);
    }
}

uint8_t* tsensor_addr(uint8_t id)
{
    return tsensor.addrs[id];
}

bool tsensor_is_overheat(void)
{
    return tsensor.is_overheat;
}

bool tsensor_is_overrange(void)
{
    return tsensor.is_overrange;
}

bool tsensor_is_errors(void)
{
    for(uint8_t i = 0; i < NUM_TEMP_SENSORS; ++i)
    {
        if((tsensor.active_state & (1 << i)) && (tsensor.addrs[i][0] == 0x28)) // активен и назначен
        {
            uint16_t val = tsensor.val[i];
            if(val & 0xF000)
            {
                return true;
            }
        }
    }
    return false;
}

bool tsensor_check_temp(void)
{
    bool is_overheat = false;
    bool is_overrange = false;
    for(uint8_t i = 0; i < NUM_TEMP_SENSORS; ++i)
    {
        uint16_t val = tsensor.val[i];
        if(!(val & 0xF000) && (tsensor.active_state & (1 << i)))
        {
            if((val >> 4) > TSENSOR_OVR_VALUE)
            {
                is_overheat = true;
                errors_on_error(ACC_OVERHEAT);
                break;
            }
            if((val >> 4) > TSENSOR_RANGE_VALUE)
            {
                is_overrange = true;
                errors_on_error(ACC_OVERHEAT);
                break;
            }
        }
    }
    tsensor.is_overheat = is_overheat;
    tsensor.is_overrange = is_overrange;

    return is_overheat | is_overrange;
}

void tsensors_init(void)
{
    memset(tsensor.rx_buffer, 0, sizeof(tsensor.rx_buffer));
    for(uint8_t i = 0; i < NUM_TEMP_SENSORS; ++i)
    {
        tsensor.val[i] = 0xF000;
    }
    tsensor.busy_count = 0;
    tsensor.current = 0;
    tsensor.rx_idx = 0;
    tsensor.tx_idx = 0;
    ds2482_init();
    memset(tsensor.addrs, 0, sizeof(tsensor.addrs));

    error_t ret = set_config();
    if(ret)
        on_error(ret);
}
