#include "stm32f4xx.h"
#include "shot.h"
#include "pcf8574.h"
#include "ili9341.h"
#include "cmsis_os.h"
#include "ai_control.h"
#include "tsensor.h"
#include "ds2482_100.h"
#include "errors.h"
#include "shot_gui.h"
#include "options.h"
#include <stdio.h>
#include <string.h>
#include "kbd.h"
#include "rom_data.h"

shot_t shot;

void disconnect_battery(void)
{
    WRITE_PIN(PCF_BATTERY_CONNECT, PIN_SET);
}

void check_battery_connection(void)
{
    if(READ_PIN(PCFIN_BATTERY_ENABLED) == PIN_SET)
    {
        shot.battery.status = BAT_NOT_CONNECTED;
    }
}

void connect_battery(void)
{
    if(ai_mean_output_voltage(5) > AI_VOLTAGE_190V)
    {
        WRITE_PIN(PCF_BATTERY_CONNECT, PIN_RESET);
        soft_timer(2, check_battery_connection, true, true);
    }else{
        shot.battery.status = BAT_NOT_CONNECTED;
        shot.measure_status |= LOW_OUTPUT_VOLTAGE_FLAG;
    }
}

// РЕЖИМ означает выход источника питания - пониженный 183В или повышенный 245В
// режим разряда это режим содержания
// переключение двух выходов происходит строго в определенной последовательности
// выкл PCFOUT_LOW_VOLTAGE_ENABLE -> вкл PCFOUT_HIGH_VOLTAGE_ENABLE
// выкл PCFOUT_HIGH_VOLTAGE_ENABLE -> вкл PCFOUT_LOW_VOLTAGE_ENABLE
static void set_mode(charge_mode_t mode)
{
    shot.mode = mode;
    switch(mode)
    {
        case CHARGING: // В режиме «Заряд» замыкается на GND выходной ключ «Сигнал повышения напряжения».
            WRITE_PIN(PCFOUT_LOW_VOLTAGE_ENABLE, PIN_SET);    // выкл
            WRITE_PIN(PCFOUT_HIGH_VOLTAGE_ENABLE, PIN_RESET); // вкл
            shot.battery.status = BAT_CHARGING;
            break;
        case INIT_MODE:
        case MAINTENANCE:
            WRITE_PIN(PCFOUT_HIGH_VOLTAGE_ENABLE, PIN_SET); // выкл // одновременно нельзя включать !!!!
            WRITE_PIN(PCFOUT_LOW_VOLTAGE_ENABLE, PIN_SET); // выкл
            shot.battery.status = BAT_MAINTENANCE;
            break;

        case CHECK_BATTERY:
            WRITE_PIN(PCFOUT_LOW_VOLTAGE_ENABLE, PIN_SET);  // выкл
            WRITE_PIN(PCFOUT_HIGH_VOLTAGE_ENABLE, PIN_RESET); // вкл
            shot.battery.status = BAT_CHECK;
            break;
        default:
            break;
    }
}

// Кнопка переключения режимов
void process_mode_button(void)
{
    int8_t button_status = READ_PIN(PCFIN_MODE_BUTTON);
    if(button_status == PIN_RESET)
    {
#ifdef SLEEP_ENABLED
        if(shot.lcd.is_sleep)
        {
            ILI9341_SleepOut();
            shot.lcd.is_sleep = false;
            shot.lcd.sleep_counter = 0;
        }
#endif
        shot.mode_switch = true;
    }
}

void process_feeders(void)
{
    shot.control_status &= ~(FEEDER1_FLAG |
                             FEEDER2_FLAG |
                             FEEDER3_FLAG);

    if((READ_PIN(PCFIN_FAULT_INPUT_1) == PIN_RESET)
    || (READ_PIN(PCFIN_VOLTAGE_INPUT_1) == PIN_RESET)
    || (READ_PIN(PCFIN_ENABLE_INPUT_1) == PIN_RESET))
    {
        shot.control_status |= FEEDER1_FLAG; // алярм
    }else{
        shot.control_status &= ~FEEDER1_FLAG;
    }

    if((READ_PIN(PCFIN_FAULT_INPUT_2) == PIN_RESET)
    || (READ_PIN(PCFIN_VOLTAGE_INPUT_2) == PIN_RESET)
    || (READ_PIN(PCFIN_ENABLE_INPUT_2) == PIN_RESET))
    {
        shot.control_status |= FEEDER2_FLAG; // алярм
    }else{
        shot.control_status &= ~FEEDER2_FLAG;
    }

    if((READ_PIN(PCFIN_FAULT_INPUT_3) == PIN_RESET)
    || (READ_PIN(PCFIN_VOLTAGE_INPUT_3) == PIN_RESET)
    || (READ_PIN(PCFIN_ENABLE_INPUT_3) == PIN_RESET))
    {
        shot.control_status |= FEEDER3_FLAG; // алярм
    }else{
        shot.control_status &= ~FEEDER3_FLAG;
    }

    // feeder_priority; 
    // 0 - равный приоритет
    // 1 - приоритет первого
    // 2 - приоритет второго
    // 3 - приоритет третьего
    // 4 - force 1 prio
    // 5 - force 2 prio
    // 6 - force 3 prio
    if(READ_PIN(PCFIN_FORCE_INPUT_1) == PIN_RESET)
    {
        shot.feeder_priority |= (1 << 1);
    }else{
        shot.feeder_priority &= ~(1 << 1);
    }
    if(READ_PIN(PCFIN_FORCE_INPUT_2) == PIN_RESET)
    {
        shot.feeder_priority |= (1 << 2);
    }else{
        shot.feeder_priority &= ~(1 << 2);
    }
    // if(READ_PIN(PCFIN_FORCE_INPUT_3) == PIN_RESET)
    // {
    //     shot.feeder_priority |= (1 << 3);
    // }else{
    //     shot.feeder_priority &= ~(1 << 3);
    // }

    if(READ_PIN(PCFIN_PRIORITY_INPUT_1) == PIN_RESET)
    {
        shot.feeder_priority |= (1 << 4);
    }else{
        shot.feeder_priority &= ~(1 << 4);
    }
    if(READ_PIN(PCFIN_PRIORITY_INPUT_2) == PIN_RESET)
    {
        shot.feeder_priority |= (1 << 5);
    }else{
        shot.feeder_priority &= ~(1 << 5);
    }
    // if(READ_PIN(PCFIN_PRIORITY_INPUT_3) == PIN_RESET)
    // {
    //     shot.feeder_priority |= (1 << 3);
    // }else{
    //     shot.feeder_priority &= ~(1 << 3);
    // }
}

void shot_process_input(void)
{
    process_feeders();
    // clear flags that will bw updated in this function
    shot.control_status &= ~(   INSUL_CONTROL_ENABLED_FLAG |
                                INSUL_CONTROL_FLAG |
                                CIRCUIT_BREAKERS_U_FLAG |
                                CIRCUIT_BREAKERS_I_FLAG |
                                CHARGER1_FLAG |
                                CHARGER2_FLAG |
                                CHARGER3_FLAG);

    if(READ_PIN(PCFIN_AMPERAGE_FAULT) == PIN_RESET)
    {
        shot.control_status |= CIRCUIT_BREAKERS_I_FLAG; // алярм
    }else{
        shot.control_status &= ~CIRCUIT_BREAKERS_I_FLAG;
    }

    if(READ_PIN(PCFIN_VOLTAGE_FAULT) == PIN_RESET)
    {
        shot.control_status |= CIRCUIT_BREAKERS_U_FLAG; // алярм
    }else{
        shot.control_status &= ~CIRCUIT_BREAKERS_U_FLAG;
    }

    if((READ_PIN(PCFIN_CHARGER_STATUS_1) == PIN_RESET))
    {
        shot.control_status |= CHARGER1_FLAG; // источники питания не в порядке
    }else{
        shot.control_status &= ~CHARGER1_FLAG; // источники питания в порядке
    }

    if((READ_PIN(PCFIN_CHARGER_STATUS_2) == PIN_RESET))
    {
        shot.control_status |= CHARGER2_FLAG; // источники питания не в порядке
    }else{
        shot.control_status &= ~CHARGER2_FLAG; // источники питания в порядке
    }

    if((READ_PIN(PCFIN_CHARGER_STATUS_3) == PIN_RESET))
    {
        shot.control_status |= CHARGER3_FLAG; // источники питания не в порядке
    }else{
        shot.control_status &= ~CHARGER3_FLAG; // источники питания в порядке
    }

    if(READ_PIN(PCFIN_INSULATION_AND_RING_ENABLE) == PIN_RESET)
    {
        shot.control_status |= INSUL_CONTROL_ENABLED_FLAG;
    }else{
        shot.control_status &= ~INSUL_CONTROL_ENABLED_FLAG;
    }

    if(shot.control_status & INSUL_CONTROL_ENABLED_FLAG)
    {
        if(READ_PIN(PCFIN_INSULATION_FAULT) == PIN_RESET)
        {
            shot.control_status |= INSUL_CONTROL_FLAG; // контроль изоляции не в норме
        }else{
            shot.control_status &= ~INSUL_CONTROL_FLAG; // контроль изоляции в норме
        }
    }

    if(READ_PIN(PCF_BATTERY_CONNECT) == PIN_RESET)
    {
        if(READ_PIN(PCFIN_BATTERY_ENABLED) == PIN_RESET) // батарея включилась
        {
            shot.measure_status &= ~BATTERY_DAMAGED_FLAG;
        }else{
            shot.measure_status |= BATTERY_DAMAGED_FLAG;
        }
    }else{
        shot.measure_status |= BATTERY_DAMAGED_FLAG;
    }
}

void shot_process_output(void)
{
    bool is_alarm = false;
    // INSULATION LAMP
    if(shot.control_status & INSUL_CONTROL_ENABLED_FLAG)
    {
        WRITE_PIN(LAMP_INSULATION_CONTROL_DISABLED, PIN_SET);
    }else{
        WRITE_PIN(LAMP_INSULATION_CONTROL_DISABLED, PIN_RESET);
    }
    // WARNING LAMP
    if((shot.control_status & ( CIRCUIT_BREAKERS_I_FLAG
                              | CIRCUIT_BREAKERS_U_FLAG))
        || (shot.measure_status &
                                TEMP_SENSOR_WARNING_FLAG
                                ))
    {
        is_alarm = true;
        WRITE_PIN(LAMP_WARNING, PIN_RESET); // вкл
    }else{
        WRITE_PIN(LAMP_WARNING, PIN_SET);
    }
    // FAULT LAMP
    if((shot.control_status &   ( INSUL_CONTROL_FLAG
                                | CIRCUIT_BREAKERS_I_FLAG
                                | CIRCUIT_BREAKERS_U_FLAG
                                | CHARGER1_FLAG
                                | CHARGER2_FLAG
                                | CHARGER3_FLAG
                                | FEEDER1_FLAG
                                | FEEDER2_FLAG
                                | FEEDER3_FLAG
                                ))
        || (shot.measure_status &
                                ( BATTERY_LOW_VOLTAGE_FLAG
                                | BATTERY_LOW_AMPERAGE_FLAG
                                | BATTERY_DAMAGED_FLAG
                                | ACCUM_OVERCHARGE_FLAG
                                | ACCUM_DIFFERENT_CHARGE_FLAG
                                | ACCUM_LOW_VOLTAGE_FLAG
                                | TEMP_SENSOR_ERROR_FLAG
                                )))
    {
        is_alarm = true;
        WRITE_PIN(LAMP_FAULT, PIN_RESET); // вкл
    }else{
        WRITE_PIN(LAMP_FAULT, PIN_SET);
    }

    //-------BATTERY CONNECTION---------------------
    if( shot.measure_status & (BATTERY_DAMAGED_FLAG
                             | ACCUM_DIFFERENT_CHARGE_FLAG
                             | TEMP_SENSOR_ERROR_FLAG
                             | ACCUM_OVERCHARGE_FLAG
                             | CRITICAL_LOW_VOLTAGE_FLAG))
    {
        is_alarm = true;
        WRITE_PIN(PCF_BATTERY_CONNECT, PIN_SET);
    }else{
        WRITE_PIN(PCF_BATTERY_CONNECT, PIN_RESET); // вкл
    }

    if(shot.measure_status & DISCHARGING_FLAG)
    {
        WRITE_PIN(LAMP_ACCUM_BAT_SUPPLY_ENABLED, PIN_RESET);
    }else{
        WRITE_PIN(LAMP_ACCUM_BAT_SUPPLY_ENABLED, PIN_SET);
    }
    //-------ALARM CALLED ONCE---------------------------------
    if(is_alarm)
    {
        WRITE_PIN(RING_THE_ALARM, PIN_RESET);
    }else{
        WRITE_PIN(RING_THE_ALARM, PIN_SET);
    }
}

void shot_ready_blink(void)
{
    TOGGLE_PIN(LAMP_CONTROLLER_READY);
    if(shot.control_status & LAMP_ACCUM_BAT_BLINK_FLAG)
        TOGGLE_PIN(LAMP_ACCUM_BAT_SUPPLY_ENABLED);
}

void shot_battery_check_enable(void)
{
    shot.mode = CHECK_BATTERY;
    shot.battery.status = BAT_CHECK;
}

void shot_battery_check_disable(void)
{
    shot.mode = MAINTENANCE;
    shot.battery.status = BAT_INIT;
}

uint32_t shot_time_to_full_charge(void)
{
    return 0;
}

uint32_t shot_time_to_power_off(void)
{
    return 0;
}

battery_status_t shot_battery_status(void)
{
    return shot.battery.status;
}

uint32_t shot_battery_level(void)
{
    return 0;
}

bool shot_can_check_battery(void)
{
    if((ai_charge_amperage() > AI_AMPERAGE_ZERO)
    & (!shot.measure_status))
    {
        return true;
    }
    return false;
}

void shot_check_temp(void)
{
    shot.measure_status &= ~TEMP_SENSOR_ERROR_FLAG;
    if(ds2482_is_error())
    {
        shot.measure_status |= TEMP_SENSOR_ERROR_FLAG;
        if(shot.mode == CHARGING)
            shot.pending_mode = MAINTENANCE;
    }

    if(tsensor_check_temp())
    {
        if(tsensor_is_overrange())
        {
            shot.measure_status |= TEMP_SENSOR_WARNING_FLAG;
            if(shot.mode == CHARGING)
                shot.pending_mode = MAINTENANCE;
        }
        if(tsensor_is_overheat())
        {
            shot.measure_status |= TEMP_SENSOR_ERROR_FLAG;
            if(shot.mode == CHARGING)
                shot.pending_mode = MAINTENANCE;
        }
    }
}

void shot_battery_status_update(void)
{
    // BAT_NOT_CONNECTED,  // Батарея не включилась    DO15=вкл && DI20=1              белая надпись на красном фоне
    // BAT_SWITCHED_OFF,   // Батарея отключена        DO15=выкл                       белая надпись на красном фоне
    // BAT_BROKEN,         // Батарея оборвана         DO15=1 && Iзаряда=[-0.5..+0.5]B белая надпись на красном фоне
    // BAT_LOW,            // Батарея разряжена        U < 183 В,                      красная надпись на жёлтом фоне
    // BAT_DISCHARGING,    // Батарея разряжается      Iзаряда<-0.1A,                  красная надпись на белом фоне
    // BAT_CHARGING,       // Батарея заряжается       режим заряда                    чёрная надпись на жёлтом фоне
    // BAT_MAINTENANCE,    // Режим сохранения         режим сохранения                чёрная надпись на белом фоне
    // BAT_CHECK,          // Проверка батареи         режим проверки                  чёрная надпись на белом фоне
    // BAT_INIT
    if(READ_PIN(PCF_BATTERY_CONNECT) == PIN_SET)
    {
        shot.battery.status = BAT_SWITCHED_OFF;
        return;
    }
    if(READ_PIN(PCFIN_BATTERY_ENABLED) == PIN_SET)
    {
        shot.battery.status = BAT_NOT_CONNECTED;
        return;
    }

    float Ubat = ai_battery_voltage();
    if(Ubat < 205.0)
    {
        shot.battery.status = BAT_LOW;
        return;
    }

    float Ibat = ai_mean_charge_amperage(2);
    if((Ibat < 0.5) && (Ibat > -0.5))
    {
        shot.battery.status = BAT_BROKEN;
        return;
    }
    if(Ibat < -0.1)
    {
        shot.battery.status = BAT_DISCHARGING;
        return;
    }else{
        switch(shot.mode)
        {
            case CHARGING:
                shot.battery.status = BAT_CHARGING;
                return;
            case MAINTENANCE:
                shot.battery.status = BAT_MAINTENANCE;
                return;
            case CHECK_BATTERY:
                shot.battery.status = BAT_CHECK;
                return;
            default:
                break;
        }
    }
}

// при пониженном напряжении
void shot_check_battery(void)
{
    float Ibat = ai_mean_charge_amperage(2);
    if((Ibat < 0.5) && (Ibat > -0.5))
    {
        shot.measure_status |= BATTERY_LOW_AMPERAGE_FLAG | BATTERY_DAMAGED_FLAG;
    }else{
        shot.measure_status &= ~(BATTERY_LOW_AMPERAGE_FLAG | BATTERY_DAMAGED_FLAG);
    }

    float Ubat = ai_mean_output_voltage(16);
    if(Ubat < 205.0)
    {
        shot.control_status |= LAMP_ACCUM_BAT_BLINK_FLAG;
        shot.measure_status |= BATTERY_LOW_VOLTAGE_FLAG;
    }else{
        shot.control_status &= ~LAMP_ACCUM_BAT_BLINK_FLAG;
        shot.measure_status &= ~BATTERY_LOW_VOLTAGE_FLAG;
    }
}

static void check_charge(void)
{
    // battery_status_t bs = BAT_MAINTENANCE;
    float Ibat = ai_mean_charge_amperage(2);
    float Ibat64 = ai_mean_charge_amperage(64);
    if(Ibat64 < ai_Izmin())
    {
        shot.pending_mode = MAINTENANCE;
    }
    if(Ibat < -0.1)
    {
        shot.measure_status |= DISCHARGING_FLAG;
    }else{
        shot.measure_status &= ~DISCHARGING_FLAG;
        ai_battery_capacity_inc(Ibat);
    }

    float Uout = ai_mean_output_voltage(255);
    if(Uout < AI_VOLTAGE_181V)
    {
        shot.measure_status |= BATTERY_LOW_VOLTAGE_FLAG | CRITICAL_LOW_VOLTAGE_FLAG; // lead to swith off battery
    }

    if(shot.measure_status & ACCUM_FULL_CHARGED_FLAG)
        shot.pending_mode = MAINTENANCE;
    float Ubat = ai_battery_voltage();
    if(Ubat > BATTERY_MAX_VOLTAGE)
        shot.pending_mode = MAINTENANCE;
}

static void check_maintenance(void)
{
    float Ibat = ai_mean_charge_amperage(2);
    float Ibat32 = ai_mean_charge_amperage(32);

    if(Ibat32 > ai_Izmin())
    {    
        if(!(shot.measure_status & (  TEMP_SENSOR_WARNING_FLAG
                                    | TEMP_SENSOR_ERROR_FLAG
                                    | ACCUM_OVERCHARGE_FLAG
                                    | ACCUM_DIFFERENT_CHARGE_FLAG))
            && !shot.accum_overcharge && !shot.current_accum_overcharge)
        {
            shot.pending_mode = CHARGING;
        }
    }
    if(Ibat < -0.1)
    {
        ai_battery_capacity_dec(Ibat);
        float Ubat = ai_battery_voltage();
        if(Ubat <= AI_VOLTAGE_183V)
        {
            shot.control_status |= LAMP_ACCUM_BAT_BLINK_FLAG;
            shot.measure_status |= BATTERY_LOW_VOLTAGE_FLAG;
        }else{
            shot.control_status &= ~LAMP_ACCUM_BAT_BLINK_FLAG;
            shot.measure_status &= ~BATTERY_LOW_VOLTAGE_FLAG;
        }
    }

    float Uout = ai_mean_output_voltage(255);
    if(Uout < AI_VOLTAGE_181V)
    {
        shot.measure_status |= BATTERY_LOW_VOLTAGE_FLAG | CRITICAL_LOW_VOLTAGE_FLAG; // lead to swith off battery
    }
}

charge_mode_t shot_mode(void)
{
    return shot.mode;
}

void shot_process_status(void)
{
    if(shot.measure_status & BATTERY_DAMAGED_FLAG)
        return;

    if(shot.measure_status & (BATTERY_LOW_VOLTAGE_FLAG))
    {
        if(!(shot.measure_status & (  TEMP_SENSOR_WARNING_FLAG
                                    | TEMP_SENSOR_ERROR_FLAG
                                    | ACCUM_OVERCHARGE_FLAG
                                    | ACCUM_DIFFERENT_CHARGE_FLAG
                                    | ACCUM_FULL_CHARGED_FLAG
                                    | CRITICAL_LOW_VOLTAGE_FLAG // leads to battery off
                                    ))
            && !shot.accum_overcharge && !shot.current_accum_overcharge)
        {
            shot.pending_mode = CHARGING;
        }
    }
}

void shot_process_mode(void)
{
    if(shot.measure_status & BATTERY_DAMAGED_FLAG)
        return;

    if(shot.mode == CHECK_BATTERY)
        return;

    if(shot.mode_switch) // по кнопке
    {
        if(shot.mode != MAINTENANCE)
            set_mode(MAINTENANCE);
        else
            set_mode(CHARGING);
        shot.mode_switch = false;
    }

    switch(shot.mode)
    {
        case CHARGING:
            check_charge();
            break;
        case MAINTENANCE:
            check_maintenance();
            break;
        case CHECK_BATTERY: // see in battery task
        case INIT_MODE:
        default:
            break;
    }

    if(shot.pending_mode != INIT_MODE)
    {
        set_mode(shot.pending_mode);
        shot.pending_mode = INIT_MODE;
    }
}

uint32_t shot_time_for_save(void) // sec
{
    uint32_t cnt = shot.start_time + shot.count + (shot.count_ovr * 0xFFFFFFFF / 1000);
    return cnt;
}

void shot_set_start_time(uint32_t time) // sec
{
    shot.start_time = time;
}

void shot_operation_time(char* string)
{
    uint32_t counter = HAL_GetTick() / 1000;
    if(counter < shot.count)
    {
        ++shot.count_ovr;
    }
    shot.count = counter;

    uint32_t cnt = shot.start_time  + shot.count + (shot.count_ovr * 0xFFFFFFFF / 1000); // секунд

    uint32_t h = (uint32_t)(cnt / 3600);
    uint8_t m = (cnt - h * 3600) / 60;
    uint8_t s = cnt - h * 3600 - m * 60;
    sprintf(string, "Время работы: %lu:%02u:%02u", h, m, s);
}

uint8_t shot_control_status(void)
{
    return shot.control_status;
}

uint8_t shot_measure_status(void)
{
    return shot.measure_status;
}

uint32_t shot_accum_overcharge(void)
{
    return shot.accum_overcharge;
}

void process_overcharge(void)
{
    if(shot.mode == CHARGING)
    {
        set_mode(MAINTENANCE);
    }else{
        shot.accum_overcharge = shot.current_accum_overcharge;
        shot.measure_status |= ACCUM_OVERCHARGE_FLAG;
    }
}

void shot_check_accum_voltage(void)
{
    float max_voltage = 0.0;
    float min_voltage = 300.0;
    shot.max_delta_voltage = 0.0;

    uint32_t current_accum_overcharge = 0;

    shot.measure_status &= ~(ACCUM_LOW_VOLTAGE_FLAG | CRITICAL_LOW_VOLTAGE_FLAG);
    for(uint8_t i = 0; i < NUM_ACCUMS; ++i)
    {
        float v = ai_accum_voltage(i);
        if(v >= ACCUM_FULL_VOLTAGE)
        {
            shot.control_status |= LAMP_ACCUM_BAT_BLINK_FLAG;
            shot.measure_status |= ACCUM_FULL_CHARGED_FLAG;
        }
        if(v > ACCUM_MAX_VOLTAGE)
        {
            current_accum_overcharge |= (1 << i);
        }
        if(v < ACCUM_LOW_VOLTAGE)
        {
            shot.measure_status |= ACCUM_LOW_VOLTAGE_FLAG;
            shot.control_status |= LAMP_ACCUM_BAT_BLINK_FLAG;
        }
        if(v < ACCUM_MIN_VOLTAGE)
        {
            shot.measure_status |= CRITICAL_LOW_VOLTAGE_FLAG;
        }
        if(v > max_voltage)
        {
            max_voltage = v;
        }
        if(v < min_voltage)
        {
            min_voltage = v;
        }
    }

    if(current_accum_overcharge & shot.current_accum_overcharge)
    {
        if(!shot.check_voltage_timer)
        {
#ifndef MODULE_TESTING
            shot.check_voltage_timer = soft_timer(8, process_overcharge, true, true);
#else
            process_overcharge();
#endif
        }else{
            if(!soft_timer_is_active(shot.check_voltage_timer))
                soft_timer_restart(shot.check_voltage_timer);
        }
    }else{
        if(!shot.check_voltage_timer)
        {
            soft_timer_stop(shot.check_voltage_timer);
        }
    }
    shot.current_accum_overcharge = current_accum_overcharge;

    shot.max_delta_voltage = max_voltage - min_voltage;
    if(shot.max_delta_voltage > ACCUM_MAX_DELTA_VOLTAGE)
        shot.measure_status |= ACCUM_DIFFERENT_CHARGE_FLAG;
    else
        shot.measure_status &= ~ACCUM_DIFFERENT_CHARGE_FLAG;
}

bool shot_is_error(uint8_t flag)
{
    uint8_t pcfstatus = pcf_status();
    if(pcfstatus & PCF_INPUTS) // 0, 1, 2 - inputs, if one of it is set => error
    { // pcf in out
        return true;
    }else{
        return shot.control_status & flag;
    }
}

uint8_t shot_feeder_priority(void)
{
    return shot.feeder_priority;
}

void shot_init(void)
{
    shot.control_status = 0;
    shot.battery.max_level = 0x7FFFF;
    shot.battery.status = BAT_SWITCHED_OFF;
    shot.start_time = 0;
    shot.count = 0;
    shot.mode_switch = false;
    tsensors_init();
    // to configure pin as input you must write 1 register
    // this will avoid writing 0 to inputs
    pcf_configure(0, 0xFF); // inputs
    pcf_configure(2, 0xFF); // inputs
    pcf_configure(4, 0xFF); // inputs
    // pcf_configure(1, 0x00); // outputs
    // pcf_configure(3, 0x00); // outputs

    soft_timer_init();
    soft_timer(5, connect_battery, true, true);
    soft_timer(60, rom_write_time, false, false);
}
