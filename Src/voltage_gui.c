#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "ugui.h"
#include "voltage_gui.h"
#include "shot.h"
#include "pcf8574.h"
#include "ai_control.h"

typedef struct
{
    int8_t  view_index; // view from
    bool    update_view;
    int8_t  old_selection;
    int8_t  selected;    // 0..V_CHANNELS
    uint16_t old_frame_width;
}voltage_gui_t;

voltage_gui_t voltage_gui;

void voltage_gui_prev(void)
{
    if(voltage_gui.selected > 0)
    {
        if(voltage_gui.selected > 0)
        {
            --voltage_gui.selected;
        }
    }
    voltage_gui.update_view = true;
}

void voltage_gui_next(void)
{
    if(voltage_gui.selected < V_CHANNELS - 1)
    {
        ++voltage_gui.selected;
    }
    voltage_gui.update_view = true;
}

uint8_t voltage_gui_selected_id(void) // for drawing name number
{
    return voltage_gui.selected;
}

void voltage_gui_update_view(void)
{
    voltage_gui.update_view = true;
}

void voltage_gui_draw(uint16_t x, uint16_t y)
{
    if(voltage_gui.old_selection == -1)
        voltage_gui.update_view = true;

    if(!voltage_gui.update_view)
        return;

    const int8_t selected = voltage_gui.selected;
    uint8_t old_frame_width = voltage_gui.old_frame_width;

    char string[64];
    sprintf(string, "Напряжение выхода:%4.2fV", ai_output_voltage());
    UG_PutString(x, y, string);
    y += 18;
    sprintf(string, "Ток выхода      :%4.2fA", ai_output_amperage());
    UG_PutString(x, y, string);
    y += 18;
    sprintf(string, "Напряжение аккум:%4.2fV", ai_battery_voltage());
    UG_PutString(x, y, string);
    y += 18;
    sprintf(string, "Ток заряда      :%4.2fA", ai_charge_amperage());
    UG_PutString(x, y, string);
    y += 30;

    for(uint8_t col = 0; col < 3; ++col)
    {
        const uint8_t row_sz = 6; // количество элементов в колонке
        uint16_t fcolor = UG_WindowGetForeColor();
        uint16_t bcolor = UG_WindowGetBackColor();

        for(uint8_t row = 0; row < row_sz; ++row)
        {
            uint8_t id = col * row_sz + row;
            if(id < 17)
            {
                uint16_t xe = 0;
                uint16_t xi = x + col * 76;
                uint8_t yi = y + row * 18;

                if(ai_channel_is_active(id))
                {
                    uint16_t fc = fcolor;
                    uint16_t bc = bcolor;
                    float vol = ai_accum_voltage(id);
                    int ret = sprintf(string, "%d:%4.2fV", id + 1, vol);
                    if(ret <= 0)
                        return;

                    if((vol > ACCUM_FULL_VOLTAGE) || (vol < ACCUM_LOW_VOLTAGE))
                    {
                        fc = C_WHITE;
                        bc = C_ORANGE;
                    }                
                    if((vol > ACCUM_MAX_VOLTAGE) || (vol < ACCUM_MIN_VOLTAGE) 
                    || (1 << id) & shot_accum_overcharge())
                    {
                        fc = C_WHITE;
                        bc = C_RED;
                    }
                    UG_SetForecolor(fc);
                    UG_SetBackcolor(bc);
                    xe = UG_PutString(xi, yi, string);
                    UG_SetForecolor(fcolor);
                    UG_SetBackcolor(bcolor);
                }else{
                    int ret = sprintf(string, "%d:откл  ", id + 1);
                    if(ret <= 0)
                        return;
                    xe = UG_PutString(xi, yi, string);
                }

                const uint8_t height = 14;
                if(id == voltage_gui.old_selection)
                {
                    UG_DrawFrame(xi, yi, voltage_gui.old_frame_width, yi + height, bcolor);
                    UG_DrawFrame(xi - 1, yi - 1, voltage_gui.old_frame_width + 1, yi + height + 1, bcolor);
                }
                if(id == selected)
                {
                    UG_DrawFrame(xi, yi, xe, yi + height, C_BLACK);
                    UG_DrawFrame(xi - 1, yi - 1, xe + 1, yi + height + 1, C_BLACK);
                    old_frame_width = xe;
                }
            }
        }
    }

    voltage_gui.old_frame_width = old_frame_width;
    voltage_gui.update_view = false;
    voltage_gui.old_selection = selected;
}

void voltage_gui_switch_state(void) // выключение канала измерения
{
    uint8_t id = voltage_gui.selected;
    if(ai_channel_is_active(id))
        ai_channel_disable(id);
    else
        ai_channel_enable(id);
    voltage_gui.update_view = true;
}

void voltage_gui_init(void)
{
    voltage_gui.old_selection = -1;
    voltage_gui.selected = 0;
    voltage_gui.update_view = true;
}
