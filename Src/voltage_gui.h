#ifndef _VOLTAGE_GUI_
#define _VOLTAGE_GUI_ 1

#include "stm32f4xx.h"
#include <stdbool.h>

#define V_CHANNELS	17

// get
uint8_t voltage_gui_selected_id(void);
void    voltage_gui_update_view(void);
// commands
void voltage_gui_draw(uint16_t x, uint16_t y);
void voltage_gui_init(void);
void voltage_gui_update_item(void);
void voltage_gui_prev(void);
void voltage_gui_next(void);
void voltage_gui_switch_state(void);

#endif //_VOLTAGE_GUI_
