#ifndef __KBD_H__
#define __KBD_H__ 1

#define KBD_BTN1_FLAG  (1 << 0)
#define KBD_BTN2_FLAG  (1 << 1)
#define KBD_BTN3_FLAG  (1 << 2)
#define KBD_BTN4_FLAG  (1 << 3)

void kbd_init(void);
void kbd_check(void);
void kbd_set_cb_1(void (*on_button_1)(void));
void kbd_set_cb_2(void (*on_button_2)(void));
void kbd_set_cb_3(void (*on_button_3)(void));
void kbd_set_cb_4(void (*on_button_4)(void));

#endif /*__KBD_H__*/
