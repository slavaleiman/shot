#ifndef _IO_GUI_
#define _IO_GUI_ 1

#include "stm32f4xx.h"
#include <stdbool.h>

#define IO_OUTPUTS   16
#define IO_INPUTS    24

// get
uint8_t io_gui_selected_id(void);
void    io_gui_update_view(void);
// commands
void io_gui_draw(uint16_t x, uint16_t y);
void io_gui_init(void);
void io_gui_update_item(void);
void io_gui_prev(void);
void io_gui_next(void);
void io_gui_switch_state(void);

#endif //_IO_GUI_
