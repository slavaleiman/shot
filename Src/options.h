#ifndef __OPTIONS_H__
#define __OPTIONS_H__ 1

#include "stm32f4xx.h"
#include "stdbool.h"

#define OPTIONS_ITEMS_ON_PAGE       10

/*
        How to use this module
    этот модуль абстракция списка возможностью отрисовки или с получением всех атрибутов списка
    модуль хранит выделенную опцию и callback на редактирование
    опции могут быть 2х типов int и float
        Для создания опции используется options_new_float и options_new_int
*/
typedef enum { _float, _int } option_type_t;
typedef struct option_s option_t;
struct option_s
{
    char name[16];
    char units[8]; // название единиц измерения
    char str_value[16]; // строка для отображения
    // параметр, может иметь доп опции для отображения, тоесть отображается коэффициент и соотв напряжение
    bool is_coeff;

    option_type_t type;
    union
    {
        int     ival;
        float   fval;
    }val;

    uint8_t id;
    union
    {
        int   (*icb)(option_t* self, int8_t val); // param - just indicator of increment
        float (*fcb)(option_t* self, int8_t val);
    }cb;
    option_t* next;
};

#define OPTIONS_MAX     8
typedef struct opt_list_s opt_list_t;
struct opt_list_s
{
    int8_t view_index; // view from
    bool update_view;

    int8_t old_selection;
    int8_t selected;    // selected option id

    option_t* first;
    option_t* last;

    bool is_edit;
};

void options_update_view(opt_list_t* opt_list);
void options_update_item(opt_list_t* opt_list);

void options_new_float(opt_list_t* opt_list, option_t* opt, const char* name, const char* units, float (*cb)(option_t*, int8_t), bool is_coeff);
void options_new_int(opt_list_t* opt_list, option_t* opt, const char* name, const char* units, int (*cb)(option_t*, int8_t), bool is_coeff);

void options_up(opt_list_t* opt_list);
void options_down(opt_list_t* opt_list);

void options_inc(opt_list_t* opt_list);
void options_dec(opt_list_t* opt_list);

char*   options_item_value(opt_list_t* opt_list, uint8_t str_num);
uint8_t options_item_id(opt_list_t* opt_list, uint8_t str_num);
char*   options_item_name(opt_list_t* opt_list, uint8_t str_num);
char*   options_item_units(opt_list_t* opt_list, uint8_t str_num);

uint8_t options_selected_id(opt_list_t* opt_list);

bool options_item_is_coeff(opt_list_t* opt_list, uint8_t str_num);
bool options_is_edit(opt_list_t* opt_list);
void options_edit(opt_list_t* opt_list, bool is_edit);
void options_update(opt_list_t* opt_list);
void options_init(opt_list_t* opt_list);

#endif // __OPTIONS_H__
