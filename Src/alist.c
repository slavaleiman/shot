#include "alist.h"
#include "FreeRTOS.h"

void* my_malloc(size_t size)
{
	// return malloc (size);
	return pvPortMalloc(size);
}

void my_free(void *ptr)
{
	// free(ptr);
	vPortFree(ptr);
}

alist_t * alist_init(void)
{
    alist_t *list = my_malloc(sizeof(alist_t));
    list->current = NULL;
    list->first = NULL;
    list->size = 0;
    return list;
}

void alist_destroy(alist_t *list)
{
    my_free(list/*, sizeof(alist_t)*/);
}

inline void alist_first(alist_t *list)
{
    list->current = list->first;
}

inline void alist_next(alist_t *list)
{
    if(list->current)
    {
        list->current = list->current->next;
    }
}

inline int alist_eol(alist_t *list)
{
    return (list->current == NULL);
}

inline int alist_is_data(alist_t *list)
{
    return (list->current != NULL);
}

inline void * alist_data(alist_t *list)
{
    return list->current->data;
}

inline size_t alist_size(alist_t *list)
{
    return list->size;
}

void alist_insert_head(alist_t *list, void *data)
{
    item_t *item = my_malloc(sizeof(item_t));
    item->data = data;
    item->next = NULL;
    if(!list->size)
    {
        list->first = item;
    }else{
        item->next = list->first;
        list->first = item;
    }
    ++list->size;
}

void alist_insert_tail(alist_t *list, void *data)
{
    item_t *item = my_malloc(sizeof(item_t));
    item->data = data;
    item->next = NULL;
    if(!list->size)
    {
        list->first = item;
    }else if(list->last)
    {
        item_t* sub_last = list->last;
        sub_last->next = item;
    }
    list->last = item;
    ++list->size;
}

// DO NOT USE inside alist_for macro, list->current = NULL;
void alist_remove_item(alist_t *list, void* data)
{
    //do not use lists current here
	if(!data)
        return;
    if(!list->size)
        return;
    if(list->first == NULL)
        return;
    item_t* prev = NULL;

    for(item_t* current = list->first; current != NULL; current = current->next)
    {
        if(current->data == data)
        {
            item_t* next = current->next;
            --list->size;
            if(prev)
                prev->next = next;
            if(list->first == current)
                list->first = next;
            if(list->last == current)
                list->last = prev;
            my_free(current/*, sizeof(item_t)*/);
            break;
        }
        prev = current;
    }
}

void alist_prev(alist_t *list)
{
    if(!list->current)
        return;
    if(!list->size)
        return;
    if(list->first == NULL)
        return;
    item_t* prev = NULL;

    for(item_t* current = list->first; current != NULL; current = current->next)
    {
        if(current == list->current)
        {
            break;
        }
        prev = current;
    }
    list->current = prev; // can be NULL
}
