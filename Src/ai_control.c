/*
    ANALOG INPUT MODULE
*/
#include "stm32f4xx.h"
#include <string.h>
#include <stdbool.h>
#include "ai_control.h"
#include "cmsis_os.h"
#include "errors.h"
#include "rom_data.h"
#include "shot_gui.h"
#include "options.h"
#include "udelay.h"
#include <stdlib.h>

#define     CHIP_SELECT()       HAL_GPIO_WritePin(GPIOG, GPIO_PIN_1, GPIO_PIN_RESET)
#define     CHIP_UNSELECT()     HAL_GPIO_WritePin(GPIOG, GPIO_PIN_1, GPIO_PIN_SET)

#define HSPI                hspi3
extern SPI_HandleTypeDef    HSPI;

#define ADC_TIMEOUT         200

// каналы переключаются внешними реле, посадкой пинов на землю
// поэтому каналы опрашиваются по одному
// не путать с каналами АЦП

ai_control_t ai_control;

ai_control_t ai_control_module(void)
{
    return ai_control;
}

float ai_Izmin(void)
{
    return ai_control.Izm.val.fval;
}

opt_list_t* ai_opt_list(void)
{
    return &ai_control.opt_list;
}

float ai_charge_amperage(void)
{
    return ai_control.charge_amperage;
}

float ai_output_amperage(void)
{
    return ai_control.output_amperage;
}

float ai_output_voltage(void)
{
    return ai_control.output_voltage;
}

float ai_accum_voltage(uint8_t idx)
{
    return ai_control.accum_voltage[idx];
}

// среднее за period секунд
float ai_mean_output_voltage(uint16_t period)
{
    if(period > VOLTAGE_BUFFER_SIZE)
        period = VOLTAGE_BUFFER_SIZE;
    if(period > ai_control.voltage_buffer_fillness)
        period = ai_control.voltage_buffer_fillness;
    // буффер наполняется 1 раз в секунду
    int16_t idx = ai_control.voltage_buffer_index - period - 1;
    if(idx < 0)
        idx += VOLTAGE_BUFFER_SIZE;
    float r = 0;
    for(uint8_t i = 0; i < period; ++i)
    {
        if(idx > VOLTAGE_BUFFER_SIZE)
            idx -= VOLTAGE_BUFFER_SIZE;
        r += ai_control.voltage_buffer[idx];
        ++idx;
    }
    if(period)
        r /= period;
    else
        r = ai_control.voltage_buffer[idx];
    return r;
}

// среднее за period секунд
float ai_mean_charge_amperage(uint16_t period)
{
    if(period > AMPERAGE_BUFFER_SIZE)
        period = AMPERAGE_BUFFER_SIZE;
    if(period > ai_control.amperage_buffer_fillness)
        period = ai_control.amperage_buffer_fillness;
    // буффер наполняется 1 раз в секунду
    int16_t idx = ai_control.amperage_buffer_index - period - 1;
    if(idx < 0)
        idx += AMPERAGE_BUFFER_SIZE;
    float r = 0;
    for(uint8_t i = 0; i < period; ++i)
    {
        if(idx > AMPERAGE_BUFFER_SIZE)
            idx -= AMPERAGE_BUFFER_SIZE;
        r += ai_control.amperage_buffer[idx];
        ++idx;
    }
    if(period)
        r /= period;
    else
        r = ai_control.amperage_buffer[idx];

    return r;
}

float ai_battery_voltage(void)
{
    return ai_control.battery_voltage;
}

float ai_battery_capacity(void)
{
    return ai_control.battery_capacity; // A * sec
}

// сумма выборок для канала
uint32_t ai_adc_value_summ(uint8_t channel) // No + i
{
    if(channel > AI_ADC_MAX_NUM_CHANNELS)
        return 0;

    uint32_t sum = 0;
    for(uint8_t i = 0; i < AI_NUM_SAMPLES; ++i)
    {
        sum += ai_control.adc_value[channel][i];
    }
    return sum;
}

#define OUTPUT_PORT1_MASK   0xFFFF
#define OUTPUT_PORT2_MASK   0xF800
#define OUTPUT_PORT3_MASK   0x0001

// переключение реле сидящих на выходном порту
// в зависимости от текущего канала измерения
void switch_relay(uint8_t channel)
{
    uint32_t output = 0;
    // PE0 - PE15, PF11 - PF15, PG0
    // turn off all
    OUTPUT_PORT1 |= OUTPUT_PORT1_MASK;
    OUTPUT_PORT2 |= OUTPUT_PORT2_MASK;
    OUTPUT_PORT3 |= OUTPUT_PORT3_MASK; // predivider disable

    switch(channel)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
            // включает (открывает) 2 реле
            output = 3 << channel;
            OUTPUT_PORT1 &= ~output;
            OUTPUT_PORT2 &= ~((output >> 16) << 11);
            break;

        // Для 17 и 18 включается A0 и предделитель
        // делитель в 10 раз уменьшает значение
        case 17: // между A17-A0
        case 18: // между A18-A0
            // Enable divider for 300V
            OUTPUT_PORT3 &= ~0x1;
            output = 1 << channel;
            OUTPUT_PORT1 &= ~0x1; // вкл А0
            OUTPUT_PORT2 &= ~((output >> 16) << 11);
            break;

        // Для 19 и 20 включается по одному реле без A0 и предделитель
        // потому что они уже одним концом сидят на 0V
        case 19: // между A19-0V
        case 20: // между A20-0V
            // Enable divider for 300V
            OUTPUT_PORT3 &= ~0x1;
            output = 1 << channel;
            OUTPUT_PORT2 &= ~((output >> 16) << 11);
            break;
        default:
            break;
    }
}

void adc_start_conversion(uint8_t sample_num)
{
    uint8_t readbuff[2];
    taskENTER_CRITICAL();
    CHIP_SELECT();
    delayUS(20);

    HAL_StatusTypeDef st = HAL_SPI_Receive(&HSPI, readbuff, 2, ADC_TIMEOUT);
    if(!st)
    {
        volatile uint16_t val = readbuff[1] | (readbuff[0] << 8);
        val >>= 1;
        ai_control.adc_value[ai_control.channel][sample_num] = val;

        if(val <= 0 || val > 0xFFF)
            ai_control.status = HAL_ERROR;
        else
            ai_control.status = HAL_OK;

        if(ai_control.status != HAL_OK)
            errors_on_error(SPI_RX_ERROR);
    }else{
        errors_on_error(SPI_RX_ERROR);
        ai_control.status = HAL_ERROR;
    }

    ai_control.status = 0;

    // без этого считывается на бит меньше на данной частоте
    delayUS(20);

    CHIP_UNSELECT();
    taskEXIT_CRITICAL();
    osDelay(10);
}

void start_adc_conversion(void)
{
    switch_relay(ai_control.channel);
    // таймаут на время переключения реле 1ms
    // а также нужно время для разрядки конденсатора ацп
    osDelay(30);
    // стартуем несколько измерений
    for(uint8_t sample_num = 0; sample_num < AI_NUM_SAMPLES; ++sample_num)
    {
        adc_start_conversion(sample_num);
    }
}

// напряжение на выходе
void ai_update_voltage_buffer(void)
{
    ai_control.voltage_buffer[ai_control.voltage_buffer_index] = ai_control.output_voltage;
    ++ai_control.voltage_buffer_index;
    ai_control.voltage_buffer_index = ai_control.voltage_buffer_index % VOLTAGE_BUFFER_SIZE;
    if(ai_control.voltage_buffer_fillness < VOLTAGE_BUFFER_SIZE)
        ++ai_control.voltage_buffer_fillness;
}

void ai_update_amperage_buffer(void)
{
    ai_control.amperage_buffer[ai_control.amperage_buffer_index] = ai_control.charge_amperage;
    ++ai_control.amperage_buffer_index;
    ai_control.amperage_buffer_index = ai_control.amperage_buffer_index % AMPERAGE_BUFFER_SIZE;
    if(ai_control.amperage_buffer_fillness < AMPERAGE_BUFFER_SIZE)
        ++ai_control.amperage_buffer_fillness;
}

void update_value(uint8_t channel)
{
    if(channel < 17)
    {
        ai_control.accum_voltage[channel] = ai_control.calib_coeff_accum.val.fval * abs((int32_t)ai_adc_value_summ(channel) / AI_NUM_SAMPLES - ai_control.zero_voltage.val.ival);
    }
    if(channel == 17)
    {
        ai_control.battery_voltage = ai_control.calib_coeff_common.val.fval * abs((int32_t)ai_adc_value_summ(channel) / AI_NUM_SAMPLES - ai_control.zero_voltage.val.ival);
    }
    if(channel == 18)
    {
        ai_control.output_voltage = ai_control.calib_coeff_common.val.fval * abs((int32_t)ai_adc_value_summ(channel) / AI_NUM_SAMPLES - ai_control.zero_voltage.val.ival);
    }
    if(channel == 19)
    {
        ai_control.charge_amperage = ai_control.calib_coeff_I1.val.fval * abs((int32_t)ai_adc_value_summ(channel) / AI_NUM_SAMPLES - ai_control.zero_amperage.val.ival);
    }
    if(channel == 20)
    {
        ai_control.output_amperage = ai_control.calib_coeff_I2.val.fval * abs((int32_t)ai_adc_value_summ(channel) / AI_NUM_SAMPLES - ai_control.zero_amperage.val.ival);
    }
    shot_gui_update_voltage(); // redraw
}

void ai_set_channel(uint8_t channel)
{
    ai_control.channel = channel;
}

void ai_channel_enable(uint8_t channel) // вкл/выкл
{
    if(channel < 17)
        ai_control.active_flag |= (1 << channel);
}

void ai_channel_disable(uint8_t channel) // вкл/выкл
{
    if(channel < 17)
        ai_control.active_flag &= ~(1 << channel);
}

bool ai_channel_is_active(uint8_t channel)
{
    return ai_control.active_flag & (1 << channel);
}

void ai_start_conversion(void)
{
    while(!((1 << ai_control.channel) & ai_control.active_flag))
        ++ai_control.channel;

    start_adc_conversion();
    update_value(ai_control.channel);
    if(!ai_control.calibration)
    {
        ++ai_control.channel;
        if(ai_control.channel >= AI_ADC_MAX_NUM_CHANNELS)
            ai_control.channel = 0;
    }
}

// значение емкости батареи хранится в А*с ai_control.battery_capacity
// и при отображении переводится в A*ч
void ai_battery_capacity_inc(float a)
{
    if(ai_control.battery_capacity < ai_control.Ca10.val.fval / 3600)
        ai_control.battery_capacity += a;
}

void ai_battery_capacity_dec(float a)
{
    if(ai_control.battery_capacity > a)
        ai_control.battery_capacity -= a;
    else
        ai_control.battery_capacity = 0;
}

float ai_full_capacity(void)
{
    return ai_control.Ca10.val.fval;
}

typedef struct
{
    int zv;
    int za;
    float cci1;
    float cci2;
    float ccac;
    float ccc;
    float up;
    float uz;
    float izm;
    float ca10;
}options_t;

uint8_t ai_get_data(uint8_t* data)
{
    options_t op;
    op.zv =    ai_control.zero_voltage.val.ival;
    op.za =    ai_control.zero_amperage.val.ival;
    op.cci1 =  ai_control.calib_coeff_I1.val.fval;
    op.cci2 =  ai_control.calib_coeff_I2.val.fval;
    op.ccac =  ai_control.calib_coeff_accum.val.fval;
    op.ccc =   ai_control.calib_coeff_common.val.fval;
    op.up =    ai_control.Up.val.fval;
    op.uz =    ai_control.Uz.val.fval;
    op.izm =   ai_control.Izm.val.fval;
    op.ca10 =  ai_control.Ca10.val.fval;

    memcpy(data, &op, sizeof(options_t));

    uint8_t sz = sizeof(options_t);

    return sz;
}

void ai_set_data(uint8_t* data, uint8_t size)
{
    if(size != sizeof(options_t))
        return;

    options_t op;
    memcpy(&op, data, sizeof(options_t));

    ai_control.zero_voltage.val.ival    = op.zv;
    ai_control.zero_amperage.val.ival   = op.za;
    ai_control.calib_coeff_I1.val.fval  = op.cci1;
    ai_control.calib_coeff_I2.val.fval  = op.cci2;
    ai_control.calib_coeff_common.val.fval = op.ccc;
    ai_control.calib_coeff_accum.val.fval = op.ccac;
    ai_control.Up.val.fval              = op.up;
    ai_control.Uz.val.fval              = op.uz;
    ai_control.Izm.val.fval             = op.izm;
    ai_control.Ca10.val.fval            = op.ca10;

    options_update(&ai_control.opt_list);
}

void ai_set_defaults(void)
{
    ai_control.zero_voltage.val.ival    = CALIB_COEF_ZERO_VOLTAGE_DEFAULT;
    ai_control.zero_amperage.val.ival   = CALIB_COEF_ZERO_AMPERAGE_DEFAULT;
    ai_control.calib_coeff_I1.val.fval  = CALIB_COEF_I1_DEFAULT;
    ai_control.calib_coeff_I2.val.fval  = CALIB_COEF_I2_DEFAULT;
    ai_control.calib_coeff_common.val.fval  = CALIB_COEF_COMMON_DEFAULT;  // с делителем
    ai_control.calib_coeff_accum.val.fval   = CALIB_COEF_ACCUM_DEFAULT;    // без делителя
    ai_control.Up.val.fval              = DISCHARGED_VOLTAGE_DEFAULT;
    ai_control.Uz.val.fval              = CHARGE_VOLTAGE_DEFAULT;
    ai_control.Izm.val.fval             = MINIMAL_CHARGE_AMPERAGE;
    ai_control.Ca10.val.fval            = ACCUM_CAPACITY_DEFAULT;
}

bool ai_status(void)
{
    return (bool)ai_control.status;
}

float ai_value_on_coeff(uint8_t opt_id)
{
    switch(opt_id)
    {
        case 0: // ноль напряжения
            return ai_control.accum_voltage[0];
        case 1: // ноль тока
            return ai_control.charge_amperage;
        case 2: // датчик тока 1
            return ai_control.charge_amperage;
        case 3: // датчик тока 2
            return ai_control.output_amperage;
        case 4: // напряжение нагрузки
            return ai_control.output_voltage;
        case 5: // напряжение батареи аккумов
            return ai_control.battery_voltage;
        default:
            return false;
    }
    return true;
}

int on_int(option_t* opt, int8_t inc)
{
    if(!inc)
        return opt->val.ival;

    if(inc > 0)
    {
        opt->val.ival = opt->val.ival + 1;
    }else{
        opt->val.ival = opt->val.ival - 1;
    }
    return opt->val.ival;
}

// +/- ten thousand
float on_float_tenth(option_t* opt, int8_t inc)
{
    if(!inc)
        return opt->val.fval;

    if(inc > 0)
    {
        opt->val.fval = opt->val.fval + 0.0001f;
    }else{
        opt->val.fval = opt->val.fval - 0.0001f;
    }
    return opt->val.fval;
}

float on_float_tens(option_t* opt, int8_t inc)
{
    if(!inc)
        return opt->val.fval;

    if(inc > 0)
    {
        opt->val.fval = opt->val.fval + 0.1f;
    }else{
        opt->val.fval = opt->val.fval - 0.1f;
    }
    return opt->val.fval;
}

float on_float_int(option_t* opt, int8_t inc)
{
    if(!inc)
        return opt->val.fval;

    if(inc > 0)
    {
        opt->val.fval = opt->val.fval + 1;
    }else{
        opt->val.fval = opt->val.fval - 1;
    }
    return opt->val.fval;
}

int on_zero_voltage(option_t* opt, int8_t inc)
{
    if(!inc)
        return opt->val.ival;

    if(inc != 0)
    {
        opt->val.ival = ai_adc_value_summ(0) / AI_NUM_SAMPLES; // при калибровке ноля - замкнуть накоротко контакты
    }
    return opt->val.ival;
}

int on_zero_amperage(option_t* opt, int8_t inc)
{
    if(!inc)
        return opt->val.ival;

    if(inc != 0)
    {
        opt->val.ival = ai_adc_value_summ(19) / AI_NUM_SAMPLES; // 19 - amperage channel
    }
    return opt->val.ival;
}

int8_t channel_on_option_id(uint8_t opt_id)
{
    switch(opt_id)
    {
        case 0: // zero voltage
            return 0;
        case 1: // zero amperage
            return 19;
        case 2: // I1
            return 19;
        case 3: // I2
            return 20;
        case 4: // Uout
            return 18;
        case 5: // Ubat
            return 17;
        default:
            return -1;
    }
    return -1;
}

void ai_set_calibration(bool calib, int8_t opt_id)
{
    ai_control.calibration = calib;
    if(calib)
    {
        int8_t channel = channel_on_option_id(opt_id);
        if(channel >= 0)
        {
            ai_control.channel = channel;
            switch_relay(channel);
        }
    }
}

bool ai_is_overcharge(void)
{
    for(uint8_t i = 0; i < NUM_ACCUMS; ++i)
    {
        if(ai_control.accum_voltage[i] > ACCUM_MAX_VOLTAGE)
        {
            return true;
        }
    }
    return false;
}

bool ai_is_lowcharge(void)
{
    for(uint8_t i = 0; i < NUM_ACCUMS; ++i)
    {
        if(ai_control.accum_voltage[i] < ACCUM_MIN_VOLTAGE)
        {
            return true;
        }
    }
    return false;
}

void ai_init(void)
{
    ai_control.status = HAL_ERROR;
    ai_control.channel = 0;
    ai_control.calibration = false;
    ai_control.voltage_buffer_index = 0;
    ai_control.amperage_buffer_index = 0;
    ai_control.active_flag = ALL_CHANNEL_ENABLE;

    memset(ai_control.amperage_buffer, 0, sizeof(ai_control.amperage_buffer));
    memset(ai_control.voltage_buffer, 0, sizeof(ai_control.voltage_buffer));
    // Надо понимать что при нажатии на плюс/минус изменяется значение коэффициента, которое может влиять на полученное значение очень слабо,
    // поэтому сам коэффициент тоже нужно отображать!!!!!!!!!!
    // примерно так
    // Zero U: <значение нуля в единицах adc>
    //   полученное значение <значение adc * коэффициент>
    // Zero I: <значение нуля в единицах adc>
    //   полученное значение <значение adc * коэффициент>
    // id = 0
    options_new_int  (&ai_control.opt_list, &ai_control.zero_voltage,       "Zero U", "В",  &on_zero_voltage, true); // при нажатии на плюс происходит обнуление (копирование текущего значение в ноль)
    // id = 1
    options_new_int  (&ai_control.opt_list, &ai_control.zero_amperage,      "Zero I", "А",  &on_zero_amperage, true);
    // id = 2
    options_new_float(&ai_control.opt_list, &ai_control.calib_coeff_I1,     "I1  ", "A",  &on_float_tenth, true); // при нажатии на плюс и минус, текущее значение корректируется
    // id = 3
    options_new_float(&ai_control.opt_list, &ai_control.calib_coeff_I2,     "I2  ", "A",  &on_float_tenth, true);
    // id = 4
    options_new_float(&ai_control.opt_list, &ai_control.calib_coeff_accum,  "Uacc", "V",  &on_float_tenth, true);
    // id = 5
    options_new_float(&ai_control.opt_list, &ai_control.calib_coeff_common, "Ubat", "V",  &on_float_tenth, true);
    // id = 6
    options_new_float(&ai_control.opt_list, &ai_control.Up,                 "Up", "V",    &on_float_tens, false);
    // id = 7
    options_new_float(&ai_control.opt_list, &ai_control.Uz,                 "Uз", "V",    &on_float_tens, false);
    // id = 8
    options_new_float(&ai_control.opt_list, &ai_control.Izm,                "Iзм", "A",   &on_float_tens, false);
    // id = 9
    options_new_float(&ai_control.opt_list, &ai_control.Ca10,               "Ca10", "A*h", &on_float_int, false);

    options_init(&ai_control.opt_list);
    options_update(&ai_control.opt_list);
}
