#ifndef __DMA_USART_IDLE__
#define __DMA_USART_IDLE__ 1
#include "stm32f4xx_hal.h"

extern UART_HandleTypeDef huart3;
#define HUART &huart3

#define HAL_UART_TRANSMIT(BUF,LEN)      \
    rs485_transmit_enable();                \
    HAL_UART_Transmit(HUART, BUF, LEN, 400);

#define HAL_UART_TRANSMIT_DMA(BUF,LEN)      \
    rs485_transmit_enable();                \
    HAL_UART_Transmit_DMA(HUART, BUF, LEN);

void USART_IrqHandler(UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma);
void DMA_IrqHandler(DMA_HandleTypeDef *hdma);
void rs485_recieve_enable(void);
void rs485_transmit_enable(void);

#endif
