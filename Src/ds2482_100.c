#include <stdbool.h>
#include <string.h>
#include "stm32f4xx.h"
#include "ds2482_100.h"
#include "tsensor.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "CRC.h"

#define HI2C    hi2c3
extern I2C_HandleTypeDef HI2C;

extern SemaphoreHandle_t I2C_MUTEX;
#define i2c_mutex I2C_MUTEX

#define DS2482_ADDR                 0x30 // 0x18 << 1

#define DS2482_DEVICE_RESET         0xF0
#define DS2482_SET_READ_POINTER     0xE1
#define DS2482_WRITE_CONFIG         0xD2
#define DS2482_1WIRE_RESET          0xB4
#define DS2482_1WIRE_SINGLE_BIT     0x87
#define DS2482_1WIRE_WRITE_BYTE     0xA5
#define DS2482_1WIRE_READ_BYTE      0x96
#define DS2482_1WIRE_TRIPLET        0x78

#define POLL_LIMIT          100 // for polling 1w bus busy status
#define DS_I2C_TIMEOUT      200

error_t ds2482_reset(void);

struct
{
    uint8_t status;
    uint8_t config;
    error_t error;
    uint8_t command;
    uint8_t tx_buffer[16];      // max val 9 byte len
    uint8_t rx_byte;
    uint16_t poll;              // counter
    uint8_t crc8;

    uint8_t ROM_NO[8];          // здесь лежит найденый адрес
    int last_discrepancy;       // расхождение
    int last_family_discrepancy;
    int last_device_flag;
}ds2482;

error_t ds2482_search_triplet(int search_direction);

void docrc8(unsigned char value)
{
   ds2482.crc8 = crc_table[ds2482.crc8 ^ value];
}

static error_t transmit(uint8_t* data, uint8_t len)
{
    taskENTER_CRITICAL();
    {
        HAL_StatusTypeDef st = HAL_I2C_Master_Transmit(&HI2C, DS2482_ADDR, data, len, DS_I2C_TIMEOUT);
        if(st != HAL_OK)
        {
            ds2482.error = (error_t)(DS_ERROR + st - 1);
        }
    }    
    taskEXIT_CRITICAL();
    
    return ds2482.error;
}

static error_t receive(uint8_t* data, uint8_t len)
{
    taskENTER_CRITICAL();
    {
        volatile HAL_StatusTypeDef st = HAL_I2C_Master_Receive(&HI2C, DS2482_ADDR, data, len, DS_I2C_TIMEOUT);
        if(st != HAL_OK)
        {
            ds2482.error = (error_t)(DS_ERROR + st - 1);
        }
    }
    taskEXIT_CRITICAL();
    return ds2482.error;
}

error_t ds2482_reset(void)
{
    ds2482.command = DS2482_DEVICE_RESET;

    error_t ret = transmit(&ds2482.command, 1);
    if(ret)
        return ret;

    ret = receive(&ds2482.rx_byte, 1);
    if(ret)
        return ret;

    ds2482.status = ds2482.rx_byte;
    ds2482.error = NO_ERROR;
    return ds2482.error;
}

error_t ds2482_write_config(void)
{
    // PPM was removed from 11/09 revision
    // cAPU = 0x01 only if 1 slave on 1-w bus presented
    int c1WS, cSPU, cAPU;
    c1WS = false;   // 1-w speed if true - overdrive
    cSPU = false;   // strong pull up
    cAPU = false;

    ds2482.config = c1WS | cSPU | cAPU;

    ds2482.command = DS2482_WRITE_CONFIG;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = ds2482.config | (~ds2482.config << 4); // config has complementary input

    error_t ret = transmit(ds2482.tx_buffer, 2);
    if(ret)
        return ret;

    ret = receive(&ds2482.rx_byte, 1);
    if(ret)
        return ret;

    return ds2482.error;
}

error_t ds2482_1w_reset(void)
{
    ds2482.command = DS2482_1WIRE_RESET;
    ds2482.tx_buffer[0] = ds2482.command;

    error_t ret = transmit(ds2482.tx_buffer, 1);
    if(ret)
        return ret;

    uint32_t poll_count = 0;
    do{
        ds2482.rx_byte = 0;
        ret = receive(&ds2482.rx_byte, 1);
        if(ret)
            return ret;
        ++poll_count;
        if (poll_count >= POLL_LIMIT)
        {
            ds2482_reset();
            return DS_1W_TIMEOUT;
        }
    }while(ds2482.rx_byte & DS_STATUS_1WB);

    ds2482.status = ds2482.rx_byte;
    if(ds2482.status & DS_STATUS_PPD)
        ds2482.error = NO_ERROR;
    // else
        // ds2482.error = DS_NO_PRESENCE; // нет датчиков, это ошибка другого уровня, не конкретно драйвера TODO передать в tsensor.c

    return ds2482.error;
}

error_t ds2482_1w_write_byte(uint8_t sendbyte)
{
    ds2482.command = DS2482_1WIRE_WRITE_BYTE;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = sendbyte;
    error_t ret = transmit(ds2482.tx_buffer, 2);
    if(ret)
        return ret;

    ds2482.rx_byte = 0;
    uint16_t poll = 0;
    while(poll < POLL_LIMIT)
    {
        ret = receive(&ds2482.rx_byte, 1);
        if(ret)
            return ret;
        ++poll;
        if(!(ds2482.rx_byte & DS_STATUS_1WB))
            return NO_ERROR;
    }
    return DS_1W_TIMEOUT;
}

error_t  ds2482_1w_read_byte(uint8_t* byte)
{
    ds2482.command = DS2482_1WIRE_READ_BYTE;
    ds2482.tx_buffer[0] = ds2482.command;
    error_t ret = transmit(ds2482.tx_buffer, 1);
    if(ret)
        return ret;

    uint32_t poll_count = 0;
    do{
        ds2482.rx_byte = 0;
        ret = receive(&ds2482.rx_byte, 1);
        if(ret)
            return ret;
        ++poll_count;
        if (poll_count >= POLL_LIMIT)
        {
            ds2482_reset();
            return DS_1W_TIMEOUT;
        }
    }while(ds2482.rx_byte & DS_STATUS_1WB);

    ds2482.command = DS2482_SET_READ_POINTER;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = 0xE1; // READ DATA REGISTER
    ret = transmit(ds2482.tx_buffer, 2);
    if(ret)
        return ret;

    ds2482.rx_byte = 0;
    ret = receive(&ds2482.rx_byte, 1);
    if(ret)
        return ret;

    *byte = ds2482.rx_byte;

    return ds2482.error;
}

error_t ds2482_1w_search(void)
{
    int id_bit_number;
    int last_zero, rom_byte_number, search_result;
    int id_bit, cmp_id_bit;
    unsigned char rom_byte_mask, search_direction;

    // initialize for search
    id_bit_number = 1;
    last_zero = 0;
    rom_byte_number = 0;
    rom_byte_mask = 1;
    search_result = false;
    ds2482.crc8 = 0;
    error_t ret = NO_ERROR;

    memset(ds2482.ROM_NO, 0, 8);

    // if the last call was not the last one
    if (!ds2482.last_device_flag)
    {
        // 1-Wire reset
        ret = ds2482_1w_reset();
        if(ret)
        {
            // reset the search
            ds2482.last_discrepancy = 0;
            ds2482.last_device_flag = false;
            ds2482.last_family_discrepancy = 0;
            return ret;
        }

        ret = ds2482_1w_write_byte(TSENSOR_SEARCH_ROM);
        if(ret)
            return ret;
        do
        {
            // if this discrepancy if before the Last Discrepancy
            // on a previous next then pick the same as last time
            if (id_bit_number < ds2482.last_discrepancy)
            {
                if ((ds2482.ROM_NO[rom_byte_number] & rom_byte_mask) > 0)
                    search_direction = 1;
                else
                    search_direction = 0;
            }
            else
            {
                // if equal to last pick 1, if not then pick 0
                if (id_bit_number == ds2482.last_discrepancy)
                    search_direction = 1;
                else
                    search_direction = 0;
            }

            // Perform a triple operation on the DS2482 which will perform
            // 2 read bits and 1 write bit
            ret = ds2482_search_triplet(search_direction);
            if(ret)
                return ret;
            // check bit results in status byte
            id_bit           = ((ds2482.status & DS_STATUS_SBR) == DS_STATUS_SBR);
            cmp_id_bit       = ((ds2482.status & DS_STATUS_TSB) == DS_STATUS_TSB);
            search_direction = ((ds2482.status & DS_STATUS_DIR) == DS_STATUS_DIR) ? 1 : 0;

            // check for no devices on 1-Wire
            if((id_bit) && (cmp_id_bit))
                break;
            else
            {
                if ((!id_bit) && (!cmp_id_bit) && (search_direction == 0))
                {
                    last_zero = id_bit_number;

                    // check for Last discrepancy in family
                    if(last_zero < 9)
                        ds2482.last_family_discrepancy = last_zero;
                }

                // set or clear the bit in the ROM byte rom_byte_number
                // with mask rom_byte_mask
                if (search_direction == 1)
                    ds2482.ROM_NO[rom_byte_number] |= rom_byte_mask;
                else
                    ds2482.ROM_NO[rom_byte_number] &= ~rom_byte_mask;

                // increment the byte counter id_bit_number
                // and shift the mask rom_byte_mask
                id_bit_number++;
                rom_byte_mask <<= 1;

                // if the mask is 0 then go to new SerialNum byte rom_byte_number
                // and reset mask
                if(rom_byte_mask == 0)
                {
                    docrc8(ds2482.ROM_NO[rom_byte_number]);  // accumulate the CRC
                    rom_byte_number++;
                    rom_byte_mask = 1;
                }
            }
        }
        while(rom_byte_number < 8);  // loop until through all ROM bytes 0-7

        // if the search was successful then
        if (!((id_bit_number < 65) || (ds2482.crc8 != 0)))
        {
            // search successful so set ds2482.last_discrepancy,ds2482.last_device_flag
            // search_result
            ds2482.last_discrepancy = last_zero;

            // check for last device
            if (ds2482.last_discrepancy == 0)
                ds2482.last_device_flag = true;

            search_result = true;
        }
    }

    // if no device found then reset counters so next
    // 'search' will be like a first

    if (!search_result || (ds2482.ROM_NO[0] == 0))
    {
        ds2482.last_discrepancy = 0;
        ds2482.last_device_flag = false;
        ds2482.last_family_discrepancy = 0;
        search_result = false;
        return DS_SENSOR_NOT_FOUND;
    }

    return NO_ERROR;
}

uint8_t* ds2482_rom(void)
{
    return ds2482.ROM_NO;
}

// сделать возврат ошибки и статус передавать через аргумент !!!!
error_t ds2482_search_triplet(int search_direction)
{
    int poll_count = 0;

    ds2482.command = DS2482_1WIRE_TRIPLET;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = search_direction ? 0x80 : 0x00;
    error_t ret = transmit(ds2482.tx_buffer, 2);
    if(ret)
        return ret;

    ret = receive(&ds2482.rx_byte, 1);
    if(ret)
        return ret;

    do
    {
        ret = receive(&ds2482.rx_byte, 1);
        if(ret)
            return ret;

        ds2482.status = ds2482.rx_byte;

    }while((ds2482.status & DS_STATUS_1WB) && (poll_count++ < POLL_LIMIT));

    if(poll_count >= POLL_LIMIT)
    {
        errors_on_error(DS_1W_TIMEOUT);
        ds2482_reset();
        return DS_1W_TIMEOUT;
    }
    return NO_ERROR;
}

error_t ds2482_is_error(void)
{
    return ds2482.error;
}

error_t ds2482_init(void)
{
    ds2482.error = NO_ERROR;
    ds2482.poll = 0;

    ds2482.last_discrepancy = 0;
    ds2482.last_device_flag = false;
    ds2482.last_family_discrepancy = 0;

    return ds2482_reset();
}
