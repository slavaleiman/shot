#ifndef __I2C_RESET_ERRATUM__
#define __I2C_RESET_ERRATUM__
#include "stm32f4xx_hal.h"

typedef struct
 {
    I2C_HandleTypeDef* instance;
    uint16_t sdaPin;
    GPIO_TypeDef* sdaPort;
    uint16_t sclPin;
    GPIO_TypeDef* sclPort;
} I2C_Module_t;

void I2C_ClearBusyFlagErratum(I2C_Module_t* i2c_mod, uint32_t timeout);

#endif //__I2C_RESET_ERRATUM__
